"use strict";

const Database = use("Database");
const User = use("App/Models/User");
const faker = use("chance").Chance();
const Drive = use("Drive");

class UserSeeder {
  async run() {
    await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
    await Database.raw("DELETE FROM users;");
    await Database.raw("ALTER TABLE users AUTO_INCREMENT = 1;");
    await Database.raw("DELETE FROM user_region;");
    await Database.raw("ALTER TABLE user_region AUTO_INCREMENT = 1;");
    await Database.raw("SET FOREIGN_KEY_CHECKS=1;");
    await Drive.delete("profile");

    await User.create({
      username: "superadmin",
      first_name: faker.first(),
      last_name: faker.last(),
      email: faker.email(),
      role_id: 1,
      password: "password",
    });
  }
}

module.exports = UserSeeder;
