"use strict";

const Database = use("Database");
const Municipality = use("App/Models/Municipality");
const Drive = use("Drive");
const snakeCaseKeys = use("snakecase-keys");

class MunicipalitySeeder {
  async run() {
    await Database.raw("DELETE FROM municipalities;");
    await Database.raw("ALTER TABLE municipalities AUTO_INCREMENT = 1;");

    const municipality = JSON.parse(
      await Drive.get("seeds/psgc/Municipality.json", "utf8")
    ).map(({ code, name, districtCode, provinceCode, regionCode }) => ({
      code,
      name,
      districtCode,
      provinceCode,
      regionCode,
    }));

    await Municipality.createMany(snakeCaseKeys(municipality, { deep: true }));
  }
}

module.exports = MunicipalitySeeder;
