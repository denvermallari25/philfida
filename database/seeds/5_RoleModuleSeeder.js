"use strict";

const Database = use("Database");
const RoleModuleModel = use("App/Models/RoleModule");
const Drive = use("Drive");

class RoleModuleSeeder {
  async run() {
    await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
    await Database.raw("DELETE FROM role_module;");
    await Database.raw("ALTER TABLE role_module AUTO_INCREMENT = 1;");
    await Database.raw("SET FOREIGN_KEY_CHECKS=1;");

    const roleModules = JSON.parse(
      await Drive.get("seeds/RoleModules.json", "utf8")
    );

    await RoleModuleModel.createMany(roleModules);
  }
}

module.exports = RoleModuleSeeder;
