"use strict";

const Database = use("Database");
const Region = use("App/Models/Region");
const Drive = use("Drive");
const snakeCaseKeys = use("snakecase-keys");

class RegionSeeder {
  async run() {
    await Database.raw("DELETE FROM regions;");
    await Database.raw("ALTER TABLE regions AUTO_INCREMENT = 1;");

    const region = JSON.parse(
      await Drive.get("seeds/psgc/Region.json", "utf8")
    ).map(({ code, name, regionName }) => ({
      code,
      name,
      regionName,
    }));

    await Region.createMany(snakeCaseKeys(region, { deep: true }));
  }
}

module.exports = RegionSeeder;
