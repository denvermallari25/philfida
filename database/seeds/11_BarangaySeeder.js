"use strict";

const Database = use("Database");
const Barangay = use("App/Models/Barangay");
const Drive = use("Drive");
const snakeCaseKeys = use("snakecase-keys");

class BarangaySeeder {
  async run() {
    await Database.raw("DELETE FROM barangays;");
    await Database.raw("ALTER TABLE barangays AUTO_INCREMENT = 1;");

    const barangay = JSON.parse(
      await Drive.get("seeds/psgc/Barangay.json", "utf8")
    ).map(
      ({
        code,
        name,
        cityCode,
        districtCode,
        municipalityCode,
        provinceCode,
        regionCode,
      }) => ({
        code,
        name,
        cityCode,
        districtCode,
        municipalityCode,
        provinceCode,
        regionCode,
      })
    );

    await Barangay.createMany(snakeCaseKeys(barangay, { deep: true }));
  }
}

module.exports = BarangaySeeder;
