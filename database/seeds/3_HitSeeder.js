"use strict";

const Database = use("Database");
const Drive = use("Drive");
const Helpers = use("Helpers");

class HitSeeder {
  async run() {
    await Database.raw("DELETE FROM hits;");
    await Database.raw("ALTER TABLE hits AUTO_INCREMENT = 1;");

    await Drive.delete(Helpers.tmpPath("hits.log"));
  }
}

module.exports = HitSeeder;
