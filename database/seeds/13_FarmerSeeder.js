"use strict";
const Database = use("Database");
const Drive = use("Drive");

class SurveySeeder {
    async run() {
        await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
        await Database.raw("DELETE FROM farmers;");
        await Database.raw("ALTER TABLE farmers AUTO_INCREMENT = 1;");
        await Database.raw("SET FOREIGN_KEY_CHECKS=1;");
        await Drive.delete("survey/farmer");

        await Drive.put("survey/farmer_version", "0");
    }
}

module.exports = SurveySeeder;
