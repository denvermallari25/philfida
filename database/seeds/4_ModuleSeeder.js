"use strict";

const Database = use("Database");
const ModuleModel = use("App/Models/Module");
const Drive = use("Drive");

class ModuleSeeder {
  async run() {
    await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
    await Database.raw("DELETE FROM modules;");
    await Database.raw("ALTER TABLE modules AUTO_INCREMENT = 1;");
    await Database.raw("SET FOREIGN_KEY_CHECKS=1;");

    const modules = JSON.parse(await Drive.get("seeds/Modules.json", "utf8"));

    await ModuleModel.createMany(modules);
  }
}

module.exports = ModuleSeeder;
