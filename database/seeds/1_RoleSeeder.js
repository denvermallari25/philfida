"use strict";

const Database = use("Database");
const Role = use("App/Models/Role");

class RoleSeeder {
  async run() {
    await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
    await Database.raw("DELETE FROM roles;");
    await Database.raw("ALTER TABLE roles AUTO_INCREMENT = 1;");
    await Database.raw("SET FOREIGN_KEY_CHECKS=1;");

    const roles = ["SUPER ADMIN", "ADMIN", "ENUMERATOR"];

    await Role.createMany(
      roles.map((role) => {
        return {
          role,
        };
      })
    );
  }
}

module.exports = RoleSeeder;
