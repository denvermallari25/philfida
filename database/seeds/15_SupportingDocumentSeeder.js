"use strict";
const Database = use("Database");
const Drive = use("Drive");

class SurveySeeder {
  async run() {
    await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
    await Database.raw("DELETE FROM supporting_documents;");
    await Database.raw("ALTER TABLE supporting_documents AUTO_INCREMENT = 1;");
    await Database.raw("SET FOREIGN_KEY_CHECKS=1;");
    await Drive.delete("survey/document");

    await Drive.put("survey/document_version", "0");
  }
}

module.exports = SurveySeeder;
