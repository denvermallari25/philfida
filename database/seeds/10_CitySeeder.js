"use strict";

const Database = use("Database");
const City = use("App/Models/City");
const Drive = use("Drive");
const snakeCaseKeys = use("snakecase-keys");

class CitySeeder {
  async run() {
    await Database.raw("DELETE FROM cities;");
    await Database.raw("ALTER TABLE cities AUTO_INCREMENT = 1;");

    const city = JSON.parse(
      await Drive.get("seeds/psgc/City.json", "utf8")
    ).map(
      ({ code, name, isCapital, districtCode, provinceCode, regionCode }) => ({
        code,
        name,
        isCapital: isCapital ? 1 : 0,
        districtCode,
        provinceCode,
        regionCode,
      })
    );

    await City.createMany(snakeCaseKeys(city, { deep: true }));
  }
}

module.exports = CitySeeder;
