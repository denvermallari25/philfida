"use strict";
const Database = use("Database");
const Survey = use("App/Models/Survey");
const Drive = use("Drive");
const Helpers = use("Helpers");
const fs = use("fs");
const SurveyService = use("App/Controllers/Service/SurveyService");

const { capitalCase } = use("change-case");

class SurveySeeder {
  async run() {
    await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
    await Database.raw("DELETE FROM surveys;");
    await Database.raw("ALTER TABLE surveys AUTO_INCREMENT = 1;");
    await Database.raw("DELETE FROM survey_forms;");
    await Database.raw("ALTER TABLE survey_forms AUTO_INCREMENT = 1;");
    await Database.raw("SET FOREIGN_KEY_CHECKS=1;");
    await Drive.delete("survey/form");
    await Drive.delete("survey/keys.json");

    const surveys = fs.readdirSync(Helpers.appRoot("storage/seeds/fibercrop"));

    for (const survey of surveys) {
      const name = capitalCase(survey.split(".")[0]).replace(" ", "/");
      const _questions = await Drive.get(
        Helpers.appRoot(`storage/seeds/fibercrop/${survey}`, "utf8")
      );
      const questions = JSON.parse(_questions);

      const surveyModel = await Survey.create({ name });
      const form = await surveyModel.forms().create({});

      await Drive.put(
        `survey/form/${form.reference_id}.json`,
        JSON.stringify(questions, null, 4)
      );

      await SurveyService.keyHelper(form);
    }

    await Drive.put("survey/version", "0");
  }
}

module.exports = SurveySeeder;
