"use strict";
const Database = use("Database");
const Drive = use("Drive");

class SurveySeeder {
  async run() {
    await Database.raw("SET FOREIGN_KEY_CHECKS=0;");
    await Database.raw("DELETE FROM survey_answers;");
    await Database.raw("ALTER TABLE survey_answers AUTO_INCREMENT = 1;");
    await Database.raw("SET FOREIGN_KEY_CHECKS=1;");
    await Drive.delete("survey/answer");
    await Drive.delete("survey/consolidated");

    await Drive.put("survey/answer_version", "0");
    await Drive.put("survey/update_version", "{}");
  }
}

module.exports = SurveySeeder;
