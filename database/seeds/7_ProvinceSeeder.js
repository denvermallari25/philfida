"use strict";

const Database = use("Database");
const Province = use("App/Models/Province");
const Drive = use("Drive");
const snakeCaseKeys = use("snakecase-keys");

class ProvinceSeeder {
  async run() {
    await Database.raw("DELETE FROM provinces;");
    await Database.raw("ALTER TABLE provinces AUTO_INCREMENT = 1;");

    const province = JSON.parse(
      await Drive.get("seeds/psgc/Province.json", "utf8")
    ).map(({ code, name, regionCode }) => ({
      code,
      name,
      regionCode,
    }));

    await Province.createMany(snakeCaseKeys(province, { deep: true }));
  }
}

module.exports = ProvinceSeeder;
