"use strict";

const Database = use("Database");
const District = use("App/Models/District");
const Drive = use("Drive");
const snakeCaseKeys = use("snakecase-keys");

class DistrictSeeder {
  async run() {
    await Database.raw("DELETE FROM districts;");
    await Database.raw("ALTER TABLE districts AUTO_INCREMENT = 1;");
    
    const distric = JSON.parse(
      await Drive.get("seeds/psgc/District.json", "utf8")
    ).map(({ code, name, regionCode }) => ({
      code,
      name,
      regionCode,
    }));

    await District.createMany(snakeCaseKeys(distric, { deep: true }));
  }
}

module.exports = DistrictSeeder;
