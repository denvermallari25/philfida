'use strict'

const Schema = use('Schema')

class FarmerSchema extends Schema {
  up() {
    this.create('farmers', (table) => {
      table.increments()

      table.string("reference_id").index();

      table.string("original_id").index();

      table.integer("sync_user").unsigned().references("id").inTable("users");

      table.timestamps()
    })
  }

  down() {
    this.drop('farmers')
  }
}

module.exports = FarmerSchema
