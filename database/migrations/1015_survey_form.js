"use strict";

const Schema = use("Schema");

class SurveyFormSchema extends Schema {
  up() {
    this.create("survey_forms", (table) => {
      table.increments();

      table.string("reference_id").index();

      table.integer("survey_id").unsigned().references("id").inTable("surveys");

      table.timestamps();
    });
  }

  down() {
    this.drop("survey_forms");
  }
}

module.exports = SurveyFormSchema;
