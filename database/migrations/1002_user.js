"use strict";

const Schema = use("Schema");

class UserSchema extends Schema {
  up() {
    this.create("users", (table) => {
      table.increments();

      table.string("username", 16).notNullable().unique();

      table.string("email").notNullable().index();

      table.string("first_name").notNullable();

      table.string("middle_name");

      table.string("extension_name");

      table.string("last_name").notNullable();

      table.string("password").notNullable();

      table.integer("is_password_changed").notNullable().index();

      table.integer("login_attempts").notNullable().index();

      table.string("profile_image");

      table.integer("role_id").unsigned().references("id").inTable("roles");

      table.integer("status").notNullable().index();

      table.timestamps();
    });
  }

  down() {
    this.drop("users");
  }
}

module.exports = UserSchema;
