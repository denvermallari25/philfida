"use strict";

const Schema = use("Schema");

class MunicipalitySchema extends Schema {
  up() {
    this.create("municipalities", (table) => {
      table.increments();

      table.string("code").index();

      table.string("name");

      table.string("district_code").index();

      table.string("province_code").index();

      table.string("region_code").index();

      table.timestamps();
    });
  }

  down() {
    this.drop("municipalities");
  }
}

module.exports = MunicipalitySchema;
