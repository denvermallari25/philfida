"use strict";

const Schema = use("Schema");

class UserRegionSchema extends Schema {
  up() {
    this.create("user_region", (table) => {
      table.increments();

      table.integer("user_id").unsigned().references("id").inTable("users");

      table.string("region_code").references("code").inTable("regions");

      table.timestamps();
    });
  }

  down() {
    this.drop("user_region");
  }
}

module.exports = UserRegionSchema;
