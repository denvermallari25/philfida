"use strict";

const Schema = use("Schema");

class SurveySchema extends Schema {
  up() {
    this.create("surveys", (table) => {
      table.increments();

      table.string("name").index();

      table.integer("status").index();

      table.integer("default_form_id").index();

      table.timestamps();
    });
  }

  down() {
    this.drop("surveys");
  }
}

module.exports = SurveySchema;
