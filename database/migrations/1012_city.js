"use strict";

const Schema = use("Schema");

class CitySchema extends Schema {
  up() {
    this.create("cities", (table) => {
      table.increments();

      table.string("code").index();

      table.string("name");

      table.integer("is_capital").index();

      table.string("district_code").index();

      table.string("province_code").index();

      table.string("region_code").index();

      table.timestamps();
    });
  }

  down() {
    this.drop("cities");
  }
}

module.exports = CitySchema;
