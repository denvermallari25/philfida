"use strict";

const Schema = use("Schema");

class SurveyAnswerSchema extends Schema {
  up() {
    this.create("survey_answers", (table) => {
      table.increments();

      table.string("reference_id").index();

      table
        .integer("survey_form_id")
        .unsigned()
        .references("id")
        .inTable("survey_forms");

      table.string("farm_name").notNullable();

      table.string("farmer_id").references("original_id").inTable("farmers");

      table.string("original_id").index();

      table.integer("sync_user").unsigned().references("id").inTable("users");

      table.integer("approver").nullable();

      table.timestamp("approve_date").nullable();

      table.integer("region_id").unsigned().references("id").inTable("regions");

      table.timestamp("created_at_mobile");

      table.integer("status");

      table.timestamps();
    });
  }

  down() {
    this.drop("survey_answers");
  }
}

module.exports = SurveyAnswerSchema;
