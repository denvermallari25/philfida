"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class SupportingDocumentSchema extends Schema {
  up() {
    this.create("supporting_documents", (table) => {
      table.increments();

      table.string("reference_id").index();

      table.string("original_id").index();

      table
        .string("answer_id")
        .references("original_id")
        .inTable("survey_answers");

      table.integer("sync_user").unsigned().references("id").inTable("users");

      table.timestamps();
    });
  }

  down() {
    this.drop("supporting_documents");
  }
}

module.exports = SupportingDocumentSchema;
