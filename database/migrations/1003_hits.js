"use strict";

const Schema = use("Schema");

class HitsSchema extends Schema {
  up() {
    this.create("hits", (table) => {
      table.increments();

      table.string("transaction_id").notNullable().index();

      table.string("username").notNullable().index();

      table.string("ip");

      table.text("url").notNullable();

      table.string("module").notNullable();

      table.string("method").notNullable();

      table.text("description").notNullable();

      table.integer("status").notNullable();

      table.timestamps();
    });
  }

  down() {
    this.drop("hits");
  }
}

module.exports = HitsSchema;
