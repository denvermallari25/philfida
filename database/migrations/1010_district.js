"use strict";

const Schema = use("Schema");

class DistrictSchema extends Schema {
  up() {
    this.create("districts", (table) => {
      table.increments();

      table.string("code").index();

      table.string("name");

      table.string("region_code").index();

      table.timestamps();
    });
  }

  down() {
    this.drop("districts");
  }
}

module.exports = DistrictSchema;
