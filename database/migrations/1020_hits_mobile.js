"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class HitsMobileSchema extends Schema {
  up() {
    this.create("hits_mobiles", (table) => {
      table.increments();

      table.string("transaction_id").notNullable().index();

      table.string("username").notNullable().index();

      table.string("ip");

      table.text("url").notNullable();

      table.string("module").notNullable();

      table.string("method").notNullable();

      table.text("description").notNullable();

      table.integer("status").notNullable();

      table.timestamps();
    });
  }

  down() {
    this.drop("hits_mobiles");
  }
}

module.exports = HitsMobileSchema;
