"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class SurveyUpdateSchema extends Schema {
  up() {
    this.create("survey_updates", (table) => {
      table.increments();

      table
        .string("new_id")
        .references("original_id")
        .inTable("survey_answers");

      table
        .string("old_id")
        .references("original_id")
        .inTable("survey_answers");

      table.timestamps();
    });
  }

  down() {
    this.drop("survey_updates");
  }
}

module.exports = SurveyUpdateSchema;
