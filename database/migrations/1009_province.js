"use strict";

const Schema = use("Schema");

class ProvinceSchema extends Schema {
  up() {
    this.create("provinces", (table) => {
      table.increments();

      table.string("code").index();

      table.string("name");

      table.string("region_code").index();

      table.timestamps();
    });
  }

  down() {
    this.drop("provinces");
  }
}

module.exports = ProvinceSchema;
