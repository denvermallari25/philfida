"use strict";

const Schema = use("Schema");

class RegionSchema extends Schema {
  up() {
    this.create("regions", (table) => {
      table.increments();

      table.string("code").unique().index();

      table.string("name");

      table.string("region_name");

      table.timestamps();
    });
  }

  down() {
    this.drop("regions");
  }
}

module.exports = RegionSchema;
