'use strict'

const Schema = use('Schema')

class BarangaySchema extends Schema {
  up () {
    this.create('barangays', (table) => {
      table.increments()

      table.string("code").index();

      table.string("name");

      table.string("city_code").index();
      
      table.string("municipality_code").index();
      
      table.string("district_code").index();

      table.string("province_code").index();

      table.string("region_code").index();

      table.timestamps()
    })
  }

  down () {
    this.drop('barangays')
  }
}

module.exports = BarangaySchema
