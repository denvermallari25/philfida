const { hooks } = use("@adonisjs/ignitor");
const { ioc } = use("@adonisjs/fold");

hooks.after.providersBooted(() => {
  const Validator = use("Validator");
  const SanitizorHooks = use("ValidatorHooks/Sanitizor");
  const ValidatorHooks = use("ValidatorHooks/Validator");

  for (const key in ValidatorHooks) {
    Validator.extend(key, ValidatorHooks[key]);
  }

  for (const key in SanitizorHooks) {
    Validator.sanitizor[key] = SanitizorHooks[key];
  }

  const View = use("View");
  View.global("getImage", (path) => {
    const fs = use("fs");
    const Helpers = use("Helpers");

    const b64Image = fs.readFileSync(Helpers.resourcesPath(path), {
      encoding: "base64",
    });

    return `data:image/jpeg;base64,${b64Image}`;
  });
});

hooks.before.providersBooted(() => {
  const Socket = use("Library/Socket/Singleton");
  ioc.singleton("Socket", (app) => new Socket());
});

hooks.after.httpServer(() => {
  const AuthService = use("App/Controllers/Service/AuthService");
  const ms = use("ms");

  setInterval(() => {
    AuthService.removeExpiredToken();
  }, ms("5m"));
});
