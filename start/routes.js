"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", () => {
    return "WEB API IS UP";
  });

  //Auth Collection
  Route.post("auth/logIn", "AuthController.logIn").validator("Auth/LogIn");

  Route.get("auth/getSession", "AuthController.getSession").middleware("auth");

  Route.get("auth/reconnect", "AuthController.reconnect").middleware("auth");

  Route.get("auth/logOut", "AuthController.logOut").middleware("auth");

  //Role Collection
  Route.get("role/getRoles", "RoleController.getRoles").middleware("auth");

  //User Collection
  Route.post("user/getUsers", "UserController.getUsers")
    .middleware("auth")
    .validator("User/GetUsers");

  Route.post("user/getUser", "UserController.getUser")
    .middleware("auth")
    .validator("User/GetUser");

  Route.post("user/uploadPhoto", "UserController.uploadPhoto")
    .middleware("auth")
    .validator("User/UploadPhoto");

  Route.post("user/createUser", "UserController.createUser")
    .middleware("auth")
    .validator("User/CreateUser");

  Route.post("user/updateUser", "UserController.updateUser")
    .middleware("auth")
    .validator("User/UpdateUser");

  Route.post("user/updateUserStatus", "UserController.updateUserStatus")
    .middleware("auth")
    .validator("User/UpdateUserStatus");

  Route.post("user/changePassword", "UserController.changePassword")
    .middleware("auth")
    .validator("User/ChangePassword");

  //Module Collection
  Route.post("module/getModules", "ModuleController.getModules")
    .middleware("auth")
    .validator("Module/GetModules");

  //Role Module Collection
  Route.post("roleModule/getRoleModule", "RoleModuleController.getRoleModule")
    .middleware("auth")
    .validator("RoleModule/GetRoleModule");

  Route.post(
    "roleModule/updateRoleModule",
    "RoleModuleController.updateRoleModule"
  )
    .middleware("auth")
    .validator("RoleModule/UpdateRoleModule");

  //Region Collection
  Route.post(
    "region/getRegionsRaw",
    "RegionController.getRegionsRaw"
  ).middleware("auth");

  Route.post("region/getRegions", "RegionController.getRegions")
    .middleware("auth")
    .validator("Region/GetRegions");

  Route.post("region/getRegion", "RegionController.getRegion")
    .middleware("auth")
    .validator("Region/GetRegion");

  Route.get(
    "region/getUnassignedAdministrators",
    "RegionController.getUnassignedAdministrators"
  )
    .middleware("auth")
    .validator("Region/GetUnassignedAdministrators");

  Route.post(
    "region/updateRegionAdministrator",
    "RegionController.updateRegionAdministrator"
  )
    .middleware("auth")
    .validator("Region/UpdateRegionAdministrator");

  Route.get(
    "region/getUnassignedEnumerators",
    "RegionController.getUnassignedEnumerators"
  )
    .middleware("auth")
    .validator("Region/GetUnassignedEnumerators");

  Route.post(
    "region/assignRegionEnumerator",
    "RegionController.assignRegionEnumerator"
  )
    .middleware("auth")
    .validator("Region/AssignRegionEnumerator");

  Route.post(
    "region/unassignRegionEnumerator",
    "RegionController.unassignRegionEnumerator"
  )
    .middleware("auth")
    .validator("Region/UnassignRegionEnumerator");

  //Province Collection
  Route.post(
    "province/getProvinces",
    "ProvinceController.getProvinces"
  ).middleware("auth");

  //District Collection
  Route.post(
    "district/GetDistricts",
    "DistrictController.getDistricts"
  ).middleware("auth");

  //Municipality Collection
  Route.post(
    "municipality/getMunicipalities",
    "MunicipalityController.getMunicipalities"
  ).middleware("auth");

  //City Collection
  Route.post("city/getCities", "CityController.getCities").middleware("auth");

  //Barangay Collection
  Route.post(
    "barangay/getBarangays",
    "BarangayController.getBarangays"
  ).middleware("auth");

  //Survey Collection
  Route.post("survey/getSurveyForms", "SurveyController.getSurveyForms")
    .middleware("auth")
    .validator("Survey/GetSurveyForms");

  Route.post("survey/getSurveyForm", "SurveyController.getSurveyForm")
    .middleware("auth")
    .validator("Survey/GetSurveyForm");

  Route.post("survey/createSurveyForm", "SurveyController.createSurveyForm")
    .middleware("auth")
    .validator("Survey/CreateSurveyForm");

  Route.post("survey/updateSurveyForm", "SurveyController.updateSurveyForm")
    .middleware("auth")
    .validator("Survey/UpdateSurveyForm");

  Route.post(
    "survey/updateSurveyFormStatus",
    "SurveyController.updateSurveyFormStatus"
  )
    .middleware("auth")
    .validator("Survey/UpdateSurveyFormStatus");

  Route.post(
    "survey/setDefaultSurveyForm",
    "SurveyController.setDefaultSurveyForm"
  )
    .middleware("auth")
    .validator("Survey/SetDefaultSurveyForm");

  Route.post("survey/getSurveyAnswers", "SurveyController.getSurveyAnswers")
    .middleware("auth")
    .validator("Survey/GetSurveyAnswers");

  Route.post("survey/getSurveyAnswer", "SurveyController.getSurveyAnswer")
    .middleware("auth")
    .validator("Survey/GetSurveyAnswer");

  Route.post("survey/approveSurvey", "SurveyController.approveSurvey")
    .middleware("auth")
    .validator("Survey/ApproveSurvey");

  Route.post(
    "survey/requestSurveyUpdate",
    "SurveyController.requestSurveyUpdate"
  )
    .middleware("auth")
    .validator("Survey/RequestSurveyUpdate");

  Route.get("survey/getSurveyFormKeys", "SurveyController.getSurveyFormKeys")
    .middleware("auth")
    .validator("Survey/GetSurveyFormKeys");

  Route.post("survey/surveyDelete", "SurveyController.surveyDelete")
    .middleware("auth")
    .validator("Survey/SurveyDelete");

  Route.post(
    "survey/getSurveySupportingDocuments",
    "SurveyController.getSurveySupportingDocuments"
  )
    .middleware("auth")
    .validator("Survey/GetSurveySupportingDocuments");

  //Report Collection
  Route.post("report/getAuditTrail", "ReportController.getAuditTrail")
    .middleware("auth")
    .validator("Report/GetAuditTrail");

  Route.post(
    "report/checkFarmerConsolidatedReport",
    "ReportController.checkFarmerConsolidatedReport"
  );

  Route.post("report/getFarmerInformationByRegion", "ReportController.getFarmerInformationByRegion");
  // .middleware("auth");
  // .validator("Report/CheckFarmerConsolidatedReport");

  //PDF Collection
  Route.get(
    "pdf/viewPDF/:token?/:type?/:data?/:title?",
    "PDFController.viewPDF"
  ).validator("PDF/ViewPDF");

  //Session Collection
  Route.post("session/hasSession", "SessionController.hasSession")
    .middleware("auth")
    .validator("Session/HasSession");

  Route.post("session/forceLogOut", "SessionController.forceLogOut")
    .middleware("auth")
    .validator("Session/ForceLogOut");
})
  .prefix("api/web/v1")
  .middleware(["flatten", "hits"]);

Route.group(() => {
  Route.get("/", () => {
    return "MOBILE API IS UP";
  });

  //Auth Collection
  Route.post("auth/logIn", "AuthController.logIn").validator("Auth/LogIn");

  Route.get("auth/logOut", "AuthController.logOut").middleware("auth");

  Route.get("auth/getSession", "AuthController.getSession").middleware("auth");

  //Sync Collection
  Route.post("sync/getVersion", "SyncController.getVersion")
    .middleware("auth")
    .validator("Sync/GetVersion");

  Route.get("sync/surveyForms", "SyncController.surveyForms")
    .middleware("auth")
    .validator("Sync/SurveyForms");

  Route.post("sync/syncFarmers", "SyncController.syncFarmers")
    .middleware("auth")
    .validator("Sync/SyncFarmers");

  Route.post("sync/syncAnswers", "SyncController.syncAnswers")
    .middleware("auth")
    .validator("Sync/SyncAnswers");

  Route.post(
    "sync/syncSupportingDocuments",
    "SyncController.syncSupportingDocuments"
  )
    .middleware("auth")
    .validator("Sync/SyncSupportingDocuments");

  Route.get("sync/getSurveyForUpdate", "SyncController.getSurveyForUpdate")
    .middleware("auth")
    .validator("Sync/GetSurveyForUpdate");
})
  .prefix("api/mobile/v1")
  .middleware(["flatten", "hits"]);

Route.get("/favicon.ico", ({ response }) => response.status(204).send());

Route.any("*", ({ response }) => response.status(404).send());
