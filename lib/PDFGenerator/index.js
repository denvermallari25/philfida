const puppeteer = use("puppeteer");

class PDFGenerator {
  constructor(content, format, margin, landscape) {
    this.content = content;
    this.format = format || "A4";
    this.margin = margin || {
      top: "28.346456693px",
      right: "37.795275591px",
      bottom: "28.346456693px",
      left: "37.795275591px",
    };
    this.landscape = landscape || false;
  }

  async generateBuffer() {
    const browser = await puppeteer.launch({
      headless: true,
      args: ["--no-sandbox"],
    });

    const page = await browser.newPage();

    await page.setContent(this.content, {
      waitUntil: ["load"],
    });

    const buffer = await page.pdf({
      format: this.format,
      margin: this.margin,
      landscape: true,
    });

    await browser.close();

    return buffer;
  }

  async generateFile(path) {
    const browser = await puppeteer.launch({
      headless: true,
      args: ["--no-sandbox"],
    });

    const page = await browser.newPage();

    await page.setContent(this.content, {
      waitUntil: ["load"],
    });

    await page.pdf({
      format: this.format,
      margin: this.margin,
      landscape: this.landscape,
      path,
    });

    await browser.close();
  }
}

module.exports = PDFGenerator;
