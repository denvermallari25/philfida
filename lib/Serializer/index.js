class Serializer {
  //Validation messages validator
  static validationMessages(_field) {
    const field = _field.toUpperCase();

    return {
      required: `[${field}] is required`,
      string: `[${field}] must be a string`,
      integer: `[${field}] must be an integer`,
      boolean: `[${field}] must be a boolean`,
      minLength: `[${field}] must be at least \${length} \${label}.`,
      maxLength: `[${field}] must not be more than \${length} \${label}.`,
      email: `[${field}] format is invalid.`,
      entryExistInModel: `[${field}] does not exist.`,
      entryUniqueInModel: `[${field}] already exist.`,
      inFilter: `[${field}] value must be [\${rack}]`,
      inInt: `[${field}] value must be [\${rack}]`,
      inString: `[${field}] value must be [\${rack}]`,
      inArrayString: `[${field}] array value/s must be [\${rack}]`,
      hasRowChanges: "No changes made.",
      matchRegex: `[${field}] value is invalid`,
      arrayInt: `[${field}] must be an array of numbers.`,
      arrayString: `[${field}] must be an array of strings.`,
      uuid: `[${field}] must be a valid uuid.`,
      array: `[${field}] must be an array.`,
      date: `[${field}] date format is invalid.`
    };
  }

  static generateValidationMessage(data) {
    const messages = Object.keys(data)
      .map((field) => {
        return data[field]
          .split("|")
          .map((validation) => {
            return validation.split(":")[0];
          })
          .reduce((a, b) => {
            const message = Serializer.validationMessages(field)[b];
            if (message)
              a[`${field}.${b}`] = Serializer.validationMessages(field)[b];

            return a;
          }, {});
      })
      .reduce((a, b) => {
        a = { ...a, ...b };

        return a;
      }, {});

    return messages;
  }

  //============================================================================
}

module.exports = Serializer;
