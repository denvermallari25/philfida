"use strict";
const socketIO = use("socket.io");
const Server = use("Server");
const Env = use("Env");
const { green } = require("kleur");
const path = Env.get("SOCKET_PATH", "/ws");
const AuthService = use("App/Controllers/Service/AuthService");
const camelcaseKeys = use("camelcase-keys");

const io = socketIO(Server.getInstance(), {
  path,
});

const workspaces = io.of(/^\/\w+$/);

workspaces.use(async (socket, next) => {
  if (socket.handshake.query.token) {
    if (socket.handshake.query.token == Env.get("APP_KEY")) next();
    else if (await AuthService.checkToken(socket.handshake.query.token)) next();
    else next(new Error("Authentication error"));
  } else next(new Error("Authentication error"));
});

workspaces.on("connection", (socket) => {
  socket.nsp.clients((error, clients) => {
    if (error) throw error;
  });

  const workspace = socket.nsp;

  socket.on("socket", ({ method, data }) => {
    const body = camelcaseKeys(data, { deep: true });
    workspace.emit(method, body);
    socket.disconnect();
  });
});

console.log(
  `${green("info")}: socket is running on ${Env.get("APP_URL")}${path}`
);
