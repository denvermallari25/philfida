"use strict";
const Env = use("Env");
const { Manager } = require("socket.io-client");

class Singleton {
  constructor() {
    this.manager = new Manager(Env.get("APP_URL"), {
      path: Env.get("SOCKET_PATH"),
      query: {
        token: Env.get("APP_KEY"),
      },
      reconnection: false,
      autoConnect: false,
    });
  }

  broadcastData(service, method, data) {
    let socket = this.manager.socket(`/${service}`);
    socket.connect();

    socket.emit("socket", {
      method,
      data,
    });
  }
}

module.exports = Singleton;
