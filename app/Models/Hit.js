"use strict";

const Model = use("Model");

class Hit extends Model {
    static castDates(field, value) {
        return value.format("MMMM D, YYYY - h:mm:ss A");
    }
}

module.exports = Hit;
