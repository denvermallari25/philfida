"use strict";

const Model = use("Model");

class Role extends Model {
  users() {
    return this.hasMany("App/Models/Role", "id", "user_id");
  }

  modules() {
    return this.belongsToMany(
      "App/Models/Module",
      "role_id",
      "module_code",
      "id",
      "code"
    ).pivotTable("role_module");
  }

  roleModule() {
    return this.hasMany("App/Models/RoleModule", "id", "role_id");
  }
}

module.exports = Role;
