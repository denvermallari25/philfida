"use strict";
const { v4: uuidv4 } = use("uuid");
const Model = use("Model");

class SurveyForm extends Model {
  static boot() {
    super.boot();

    this.addHook("beforeCreate", (surveyInstance) => {
      surveyInstance.reference_id = uuidv4();
    });

    this.addHook("afterCreate", async (surveyInstance) => {
      const survey = await surveyInstance.survey().first();

      survey.merge({ default_form_id: surveyInstance.id });

      await survey.save();
    });
  }

  survey() {
    return this.belongsTo("App/Models/Survey", "survey_id", "id");
  }
}

module.exports = SurveyForm;
