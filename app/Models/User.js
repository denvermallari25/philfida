"use strict";

const Model = use("Model");
const Hash = use("Hash");
const Socket = use("Socket");

class User extends Model {
  static boot() {
    super.boot();

    this.addHook("beforeSave", async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password);
      }
    });

    this.addHook("beforeCreate", (userInstance) => {
      userInstance.username = userInstance.username.toUpperCase();
      userInstance.middle_name = userInstance.middle_name || "";
      userInstance.extension_name = userInstance.extension_name || "";
      userInstance.is_password_changed = 0;
      userInstance.login_attempts = 0;
      userInstance.status = 1;
    });

    this.addHook("afterSave", (userInstance) => {
      Socket.broadcastData("User", "Save", userInstance.toJSON());
    });
  }

  static get hidden() {
    return ["password", "login_attempts", "is_password_changed"];
  }

  static get computed() {
    return ["fullName"];
  }

  getFullName({ first_name, middle_name, last_name, extension_name }) {
    let fullname = first_name;

    if (middle_name.trim().length > 0) fullname += ` ${middle_name}`;

    fullname += ` ${last_name}`;

    if (extension_name.trim().length > 0) fullname += ` ${extension_name}`;

    return fullname;
  }

  static castDates(field, value) {
    return value.format("MMMM D, YYYY - h:mm:ss A");
  }

  sessions() {
    return this.hasMany("App/Models/Session", "id", "user_id");
  }

  role() {
    return this.belongsTo("App/Models/Role", "role_id", "id");
  }

  userRegion() {
    return this.hasOne("App/Models/UserRegion");
  }

  farmers() {
    return this.hasMany("App/Models/Farmer", "id", "sync_user");
  }

  region() {
    return this.belongsToMany(
      "App/Models/Region",
      "user_id",
      "region_code",
      "id",
      "code"
    )
      .pivotTable("user_region")
      .withTimestamps();
  }
}

module.exports = User;
