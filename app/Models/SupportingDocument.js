"use strict";

const Model = use("Model");

class SupportingDocument extends Model {
  static get table() {
    return "supporting_documents";
  }
}

module.exports = SupportingDocument;
