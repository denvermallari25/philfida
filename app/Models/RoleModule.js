"use strict";

const Model = use("Model");

class RoleModule extends Model {
  static get table() {
    return "role_module";
  }

  role() {
    return this.belongsTo("App/Models/Role", "role_id", "id");
  }

  module() {
    return this.belongsTo("App/Models/Module", "module_code", "code");
  }
}

module.exports = RoleModule;
