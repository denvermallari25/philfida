"use strict";

const Model = use("Model");

class Region extends Model {
  users() {
    return this.belongsToMany(
      "App/Models/User",
      "region_code",
      "user_id",
      "code",
      "id"
    )
      .pivotTable("user_region")
      .withTimestamps();
  }
}

module.exports = Region;
