"use strict";

const Model = use("Model");
const Answer = use("App/Models/Answer");

class SurveyUpdate extends Model {
  static boot() {
    super.boot();

    this.addHook("afterCreate", async (updateInstance) => {
      const ans = await Answer.findBy("original_id", updateInstance.old_id);

      ans.merge({
        status: -2,
      });

      await ans.save();
    });
  }

  new_survey() {
    return this.belongsTo("App/Models/Answer", "new_id", "original_id");
  }

  old_survey() {
    return this.belongsTo("App/Models/Answer", "old_id", "original_id");
  }
}

module.exports = SurveyUpdate;
