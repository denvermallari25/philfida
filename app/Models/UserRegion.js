"use strict";

const Model = use("Model");

class UserRegion extends Model {
  static get table() {
    return "user_region";
  }

  user() {
    return this.belongsTo("App/Models/User", "user_id", "id");
  }

  region() {
    return this.belongsTo("App/Models/Region", "region_code", "code");
  }
}

module.exports = UserRegion;
