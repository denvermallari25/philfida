"use strict";

const Model = use("Model");

class Survey extends Model {
  static boot() {
    super.boot();

    this.addHook("beforeCreate", (surveyInstance) => {
      surveyInstance.status = 1;
    });
  }

  forms() {
    return this.hasMany("App/Models/SurveyForm", "id", "survey_id");
  }
}

module.exports = Survey;
