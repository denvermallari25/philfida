"use strict";

const Model = use("Model");

class Module extends Model {
  static boot() {
    super.boot();

    this.addHook("beforeCreate", (moduleInstance) => {
      moduleInstance.status = 1;
    });
  }

  static get hidden() {
    return ["id", "updated_at", "created_at"];
  }

  roles() {
    return this.belongsToMany(
      "App/Models/Role",
      "module_code",
      "role_id",
      "code",
      "id"
    ).pivotTable("role_module");
  }
}

module.exports = Module;
