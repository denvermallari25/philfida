"use strict";

const Model = use("Model");

class Answer extends Model {
  static boot() {
    super.boot();

    this.addHook("beforeCreate", (answerInstance) => {
      answerInstance.status = 0;
      answerInstance.approve_date = null;
      answerInstance.approver = null;
    });
  }

  static get table() {
    return "survey_answers";
  }

  static get dates() {
    return super.dates.concat(["created_at_mobile", "approve_date"]);
  }

  static castDates(field, value) {
    return value.format("MMMM D, YYYY - h:mm:ss A");
  }

  region() {
    return this.belongsTo("App/Models/Region", "region_id", "id");
  }

  enumerator() {
    return this.belongsTo("App/Models/User", "sync_user", "id");
  }

  survey_form() {
    return this.belongsTo("App/Models/SurveyForm", "survey_form_id", "id");
  }

  farmer() {
    return this.belongsTo("App/Models/Farmer", "farmer_id", "original_id");
  }

  supporting_documents() {
    return this.hasMany(
      "App/Models/SupportingDocument",
      "original_id",
      "answer_id"
    );
  }

  approve_user() {
    return this.belongsTo("App/Models/User", "approver", "id");
  }

  new_survey() {
    return this.hasOne("App/Models/SurveyUpdate", "original_id", "new_id");
  }

  old_survey() {
    return this.hasOne("App/Models/SurveyUpdate", "original_id", "old_id");
  }
}

module.exports = Answer;
