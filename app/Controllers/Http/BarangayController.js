"use strict";

const Barangay = use("App/Models/Barangay");

class BarangayController {
  async getBarangays({ request, response }) {
    const {
      regionCode,
      provinceCode,
      districtCode,
      cityCode,
      municipalityCode,
    } = request.all();

    let q = Barangay.query();

    if (regionCode) q.where("region_code", regionCode);
    if (provinceCode) q.where("province_code", provinceCode);
    if (districtCode) q.where("district_code", districtCode);
    if (cityCode) q.where("city_code", cityCode);
    // if (municipalityCode) q.where("municipality_code", municipalityCode);

    q.orderBy("id", "asc");

    const barangays = await q.fetch();
    let data;

    if (barangays.rows.length > 0)
      data = {
        message: "Successfully fetched barangays.",
        data: barangays.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }
}

module.exports = BarangayController;
