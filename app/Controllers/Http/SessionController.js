"use strict";

const User = use("App/Models/User");
const Session = use("App/Models/Session");

class SessionController {
  async hasSession({ request, response }) {
    const { username } = request.all();

    let user = await User.findBy("username", username);
    await user.load("sessions");

    let data;

    if (user.toJSON().sessions.length > 0)
      data = {
        status: 200,
        message: "User is currently logged.",
      };
    else
      data = {
        status: 404,
        message: "User is not logged.",
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }

  async forceLogOut({ request, response }) {
    const { id } = request.all();

    const session = await Session.findBy("user_id", id);

    if (session) await session.delete();

    return response.status(200).send({ message: "Successfully logged out" });
  }
}

module.exports = SessionController;
