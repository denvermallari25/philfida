const RoleModel = use("App/Models/Role");

class RoleController {
  async getRoles({ request, response }) {
    const roles = await RoleModel.all();

    return response
      .status(200)
      .send({ message: "Successfully fetched data", data: roles.toJSON() });
  }
}

module.exports = RoleController;
