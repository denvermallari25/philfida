"use strict";

const RegionService = use("App/Controllers/Service/RegionService");
const Region = use("App/Models/Region");
const User = use("App/Models/User");
const Fetch = use("Library/Fetch");
const Socket = use("Socket");

class RegionController {
  async getRegionsRaw({ response }) {
    let regions = Region.query().orderBy("id", "asc").fetch();
    let data;

    if (regions.rows.length > 0)
      data = {
        message: "Successfully fetched regions.",
        data: regions.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }

  async getRegions({ request, response }) {
    const { search, limit, page } = request.all();

    let q = Region.query().with("users").orderBy("id", "asc");

    const searchFields = RegionService.searchFields;
    const FetchInstance = new Fetch(q);
    const { status, data, ...body } = await FetchInstance.search(
      searchFields,
      search
    ).get(limit, page);

    const sanitizeData = data.map(({ users, ...d }) => ({
      ...d,
      administrator: users.find((u) => u.role_id == 2) || null,
      enumerators: users.filter((u) => u.role_id == 3),
    }));

    return response.status(status).send({ data: sanitizeData, ...body });
  }

  async getRegion({ request, response }) {
    const { id, code } = request.all();

    const q = id != undefined ? { id } : { code };

    const region = await Region.query().where(q).first();

    const administrator = await region.users().where("role_id", 2).first();
    const enumerators = await region.users().where("role_id", 3).fetch();

    const data = {
      ...region.toJSON(),
      administrator: administrator ? administrator.toJSON() : null,
      enumerators: enumerators.toJSON(),
    };

    return response.send({ data });
  }

  async getUnassignedAdministrators({ response }) {
    const admin = await RegionService.getRegionUser("administrator");

    let data;

    if (admin.rows.length > 0)
      data = {
        message: "Successfully fetched unassigned administrators.",
        data: admin.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }

  async getUnassignedEnumerators({ response }) {
    const enumerators = await RegionService.getRegionUser("enumerator");

    let data;

    if (enumerators.rows.length > 0)
      data = {
        message: "Successfully fetched unassigned enumerators.",
        data: enumerators.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }

  async updateRegionAdministrator({ request, response }) {
    const { id, administrator } = request.all();

    const region = await Region.find(id);

    await region.load("users", (b) => {
      b.where("role_id", 2);
    });

    const admin = region.getRelated("users");

    if (admin.rows.length > 0) {
      const { id: adminId } = admin.toJSON()[0];
      await region.users().detach([adminId]);
    }

    if (administrator != 0) {
      await region.users().attach([administrator]);
    }

    Socket.broadcastData("Region", "Save", region.toJSON());

    return response
      .status(200)
      .send({ message: "Successfully updated region.", data: region.toJSON() });
  }

  async assignRegionEnumerator({ request, response }) {
    const { id, enumerators } = request.all();

    const region = await Region.find(id);

    await region.users().attach(enumerators);

    Socket.broadcastData("Region", "Save", region.toJSON());

    return response.status(200).send({
      message: "Successfully assigned enumerator/s.",
      data: region.toJSON(),
    });
  }

  async unassignRegionEnumerator({ request, response }) {
    const { id, enumerators } = request.all();

    const region = await Region.find(id);

    await region.users().detach(enumerators);

    Socket.broadcastData("Region", "Save", region.toJSON());

    return response.status(200).send({
      message: "Successfully unassigned enumerator/s.",
      data: region.toJSON(),
    });
  }
}

module.exports = RegionController;
