"use strict";
const PDFService = use("App/Controllers/Service/PDFService");
const ReportService = use("App/Controllers/Service/ReportService");

class PDFController {
  async viewPDF({ params, response }) {
    const { type, data } = params;
    let pdf;

    if (type == "survey") {
      const _d = Buffer.from(params.data, "base64").toString();
      const { id } = JSON.parse(_d);

      pdf = await PDFService.generateSurvey(id);
    } else if (type == "farmerinfoconsolidation") {
      const _d = Buffer.from(params.data, "base64").toString();
      const { date, selected } = JSON.parse(_d);

      const months = ReportService.getMonths(date);
      const cdata = await ReportService.getFarmConsolidationData(
        months,
        selected
      );

      pdf = await PDFService.generateFarmerInfoConsolidation(cdata, selected);
    }

    response.header("Content-Disposition", `inline;filename="${params.title}"`);
    response.type("application/pdf");

    return response.send(pdf);
  }
}

module.exports = PDFController;
