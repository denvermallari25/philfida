"use strict";

const Province = use("App/Models/Province");

class ProvinceController {
  async getProvinces({ request, response }) {
    const { regionCode } = request.all();

    let q = Province.query();

    if (regionCode) q.where("region_code", regionCode);

    q.orderBy("id", "asc");

    const provinces = await q.fetch();
    let data;

    if (provinces.rows.length > 0)
      data = {
        message: "Successfully fetched provinces.",
        data: provinces.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }
}

module.exports = ProvinceController;
