"use strict";

const District = use("App/Models/District");

class DistrictController {
  async getDistricts({ request, response }) {
    const { regionCode } = request.all();

    let q = District.query();

    if (regionCode) q.where("region_code", regionCode);

    q.orderBy("id", "asc");

    const districts = await q.fetch();
    let data;

    if (districts.rows.length > 0)
      data = {
        message: "Successfully fetched districts.",
        data: districts.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }
}

module.exports = DistrictController;
