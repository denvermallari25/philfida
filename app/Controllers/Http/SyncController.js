"use strict";
const Survey = use("App/Models/Survey");
const Farmer = use("App/Models/Farmer");
const Answer = use("App/Models/Answer");
const SurveyUpdate = use("App/Models/SurveyUpdate");
const SupportingDocument = use("App/Models/SupportingDocument");
const Drive = use("Drive");
const { v4: uuidv4 } = use("uuid");
const FarmerService = use("App/Controllers/Service/FarmerService");
const AnswerService = use("App/Controllers/Service/AnswerService");
const SupportingDocumentService = use(
  "App/Controllers/Service/SupportingDocumentService"
);

class SyncController {
  async getVersion({ request, response, session }) {
    const { type } = request.all();
    let version;

    if (type == "survey") version = await Drive.get("survey/version", "utf8");
    else if (type == "update") {
      const _v = await Drive.get("survey/update_version", "utf8");
      const v = JSON.parse(_v);
      version =
        v[session.region[0].id] != undefined ? v[session.region[0].id] : 0;
    }

    // else if (type == "farmer")
    //   version = await Drive.get("survey/farmer_version", "utf8");
    // else if (type == "answer")
    //   version = await Drive.get("survey/answer_version", "utf8");

    const data = {
      message: `Successfully fetched ${type} version.`,
      version,
      status: 200,
    };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }

  async surveyForms({ response }) {
    let data;
    let survey = await Survey.query()
      .with("forms", (q) => {
        q.orderBy("id", "desc");
      })
      .orderBy("id", "desc")
      .fetch();

    if (survey.rows.length > 0) {
      const _survey = survey.toJSON();

      for (const [idx, s] of _survey.entries()) {
        const { forms } = s;

        _survey[idx].forms = await Promise.all(
          forms.map(async (f) => {
            const _form = await Drive.get(
              `survey/form/${f.reference_id}.json`,
              "utf8"
            );
            const form = JSON.parse(_form);

            return {
              ...f,
              questions: form,
            };
          })
        );
      }

      data = {
        message: "Successfully fetched survey forms for syncing.",
        data: _survey,
        status: 200,
      };
    } else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }

  async syncFarmers({ request, response, session }) {
    const { farmers } = request.all();

    const data = await Promise.all(
      farmers.map(async (farmer) => {
        const reference_id = uuidv4();

        await Drive.put(
          `survey/farmer/${reference_id}.json`,
          JSON.stringify(farmer, null, 4)
        );

        return { original_id: farmer.id, reference_id, sync_user: session.id };
      })
    );

    await Farmer.createMany(data);

    await FarmerService.updateVersion();

    return response.status(200).send({
      message: "Successfully synced farmers.",
      id: data
        .sort((a, b) => a.original_id - b.original_id)
        .map((d) => d.original_id),
    });
  }

  async syncAnswers({ request, response, session }) {
    const { survey_answers: surveyAnswers } = request.all();

    const data = await Promise.all(
      surveyAnswers.map(async (surveyAnswer) => {
        const reference_id = uuidv4();

        await Drive.put(
          `survey/answer/${reference_id}.json`,
          JSON.stringify(surveyAnswer.answers, null, 4)
        );

        const ret = {
          created_at_mobile: surveyAnswer.timestamp,
          farm_name: surveyAnswer.farmname,
          farmer_id: surveyAnswer.farmer_id,
          survey_form_id: surveyAnswer.survey_form_id,
          original_id: surveyAnswer.id,
          reference_id,
          sync_user: session.id,
          region_id: session.region[0].id,
        };

        if (surveyAnswer.from) {
          ret.from = {
            new_id: surveyAnswer.id,
            old_id: surveyAnswer.from,
          };
        }

        return ret;
      })
    );

    const _answers = data.map(({ from, ...d }) => d);
    const _updated = data
      .map(({ from, ...d }) => from)
      .filter((e) => e != undefined);

    const answers = await Answer.createMany(_answers);

    if (_updated.length > 0) {
      await SurveyUpdate.createMany(_updated);
    }

    await AnswerService.updateVersion(answers);

    return response.status(200).send({
      message: "Successfully synced survey answers.",
      id: data
        .sort((a, b) => a.original_id - b.original_id)
        .map((d) => d.original_id)
    });
  }

  async syncSupportingDocuments({ request, response, session }) {
    const { supporting_documents } = request.all();

    const data = await Promise.all(
      supporting_documents.map(
        async ({ surveyAnswerId, id, encodeFiles, details, type }) => {
          const reference_id = uuidv4();
          const content = {
            encodeFiles,
            details: details || null,
            type,
          };

          await Drive.put(
            `survey/document/${reference_id}.txt`,
            JSON.stringify(content, null, 4)
          );

          return {
            reference_id,
            answer_id: surveyAnswerId,
            sync_user: session.id,
            original_id: id,
          };
        }
      )
    );

    const sp = await SupportingDocument.createMany(data);

    await SupportingDocumentService.updateVersion(sp);

    return response.status(200).send({
      message: "Successfully synced supporting documents.",
      id: data
        .sort((a, b) => a.original_id - b.original_id)
        .map((d) => d.original_id),
    });
  }

  async getSurveyForUpdate({ response, session }) {
    const region = session.region[0].id;

    const answers = await Answer.query()
      .where({
        region_id: region,
        status: -1,
      })
      .fetch();

    let data;

    if (answers.rows.length > 0) {
      data = {
        message: "Successfully fetched survey/s for update.",
        ids: answers.toJSON().map((a) => a.original_id),
        status: 200,
      };
    } else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }
}

module.exports = SyncController;
