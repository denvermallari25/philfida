"use strict";

const Municipality = use("App/Models/Municipality");
const Fetch = use("Library/Fetch");

class MunicipalityController {
  async getMunicipalities({ request, response }) {
    const { regionCode, provinceCode, districtCode } = request.all();

    let q = Municipality.query();

    if (regionCode) q.where("region_code", regionCode);
    if (provinceCode) q.where("province_code", provinceCode);
    if (districtCode) q.where("district_code", districtCode);

    q.orderBy("id", "asc");

    const municipalities = await q.fetch();
    let data;

    if (municipalities.rows.length > 0)
      data = {
        message: "Successfully fetched municipalities.",
        data: municipalities.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }
}

module.exports = MunicipalityController;
