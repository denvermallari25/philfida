"use strict";
const { generateToken, getSession , removeToken } = use(
  "App/Controllers/Service/AuthService"
);

class AuthController {
  async logIn({ request, response }) {
    const { username } = request.all();

    const session = await generateToken(username);

    return response
      .status(200)
      .send({ ...session, message: "Successfully logged in." });
  }

  async getSession({ response, session }) {
    const sesh = await getSession(session.username);

    return response
      .status(200)
      .send({ ...sesh, message: "Successfully fetched session." });
  }

  async reconnect({ request, response, session }) {
    const token = request.header("Authorization").replace("Bearer ", "");
    const { user } = await getSession(session.username);

    const sesh = await generateToken(user.username);
    await removeToken(token);

    return response
      .status(200)
      .send({ ...sesh, message: "Successfully reconnected." });
  }

  async logOut({ request, response }) {
    const token = request.header("Authorization").replace("Bearer ", "");
    await removeToken(token);

    return response.status(200).send({ message: "Successfully logged out." });
  }
}

module.exports = AuthController;
