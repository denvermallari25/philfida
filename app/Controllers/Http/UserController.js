"use strict";
const UserModel = use("App/Models/User");
const Region = use("App/Models/Region");
const UserService = use("App/Controllers/Service/UserService");
const Fetch = use("Library/Fetch");
const snakeCaseKeys = use("snakecase-keys");
const Helpers = use("Helpers");
const Drive = use("Drive");
const { v5: uuidv5 } = use("uuid");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const randomstring = use("randomstring");

class UserController {
  async getUsers({ request, response, session }) {
    const { search, filter, limit, page } = request.all();

    let q = UserModel.query().with("role").orderBy("id", "desc");

    if (session.role_id == 1) q.where("role_id", "!=", 3);
    else if (session.role_id == 2)
      q.where("role_id", 3).whereHas("region", (q) => {
        q.where("regions.id", session.region[0].id);
      });

    if (filter && filter == "0") q.where("status", 1);
    else if (filter && filter == "-1") q.where("status", 0);
    else if (filter && filter == "-2") q.where("status", -1);
    else if (filter && filter != "*") q.where("role_id", +filter);

    const searchFields = UserService.searchFields;
    const FetchInstance = new Fetch(q);
    const { status, ...data } = await FetchInstance.search(
      searchFields,
      search
    ).get(limit, page);

    return response.status(status).send(data);
  }

  async getUser({ request, response }) {
    const { id, username } = request.all();

    const q = id != undefined ? { id } : { username };

    const user = await UserModel.query().where(q).with("role").first();
    
    const photo =
      user.profile_image != null
        ? `data:image/jpeg;base64,${await Drive.get(
            `profile/${user.profile_image}`,
            "base64"
          )}`
        : null;

    return response.send({ data: user.toJSON(), photo });
  }

  async createUser({ request, response }) {
    const { regionId, ...requestModel } = request.only([
      "username",
      "email",
      "firstName",
      "middleName",
      "extensionName",
      "lastName",
      "roleId",
      "regionId",
    ]);

    const password = "password";
    // const password = randomstring.generate(12);

    const data = snakeCaseKeys(requestModel, { deep: true });

    data.password = password;

    const user = await UserModel.create(data);

    if (user.role_id != 1 && regionId != 0) {
      const region = await Region.find(regionId);
      const admin = await region.users().where("role_id", 2).first();

      if (user.role_id == 2 && admin) await region.users().detach([admin.id]);

      region.users().attach([user.id]);
    }

    // await UserService.sendEmail(
    //   "create",
    //   { ...user.toJSON(), password },
    //   user.email
    // );

    return response.send({
      message: "Successfully created user.",
      data: user.toJSON(),
    });
  }

  async uploadPhoto({ request, response }) {
    const { id } = request.all();
    const profilePhoto = request.file("profile");
    const user = await UserModel.find(id);

    const filename = `${uuidv5(user.username, uuidv5.URL)}.jpg`;

    await profilePhoto.move(Helpers.appRoot(`storage/profile`), {
      name: filename,
      overwrite: true,
    });

    user.merge({ profile_image: filename });
    await user.save();

    return response.send({
      message: "Successfully uploaded photo.",
    });
  }

  async updateUser({ request, response }) {
    const { id, hasPhoto, ...requestModel } = request.only([
      "id",
      "email",
      "firstName",
      "middleName",
      "extensionName",
      "lastName",
      "hasPhoto",
    ]);

    const User = await UserModel.find(id);
    const data = snakeCaseKeys(requestModel);

    if (!hasPhoto) {
      const filePath = `profile/${User.profile_image}`;

      (await Drive.exists(filePath)) && (await Drive.delete(filePath));
      data.profile_image = null;
    }

    User.merge(data);

    await User.save();

    return response.send({
      message: "Successfully updated user.",
      data: User.toJSON(),
    });
  }

  async updateUserStatus({ request, response }) {
    const { id, status } = request.all();

    let mergeData = { status };

    const User = await UserModel.find(id);

    if (mergeData.status == 1) mergeData.login_attempts = 0;

    User.merge(mergeData);

    await User.save();

    return response.send({
      message: "Successfully updated user status.",
      data: User.toJSON(),
    });
  }

  async changePassword({ request, response }) {
    const { newPassword: password } = request.all();
    const token = request.header("Authorization").replace("Bearer ", "");
    const { user } = await getSessionByToken(token);

    const User = await UserModel.find(user.id);
    User.merge({ password, is_password_changed: 1 });

    await User.save();

    return response.send({ message: "Successfully changed password." });
  }
}

module.exports = UserController;
