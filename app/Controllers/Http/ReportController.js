"use strict";
const Hit = use("App/Models/Hit");
const ReportService = use("App/Controllers/Service/ReportService");
const FarmerService = use("App/Controllers/Service/FarmerService");
const Fetch = use("Library/Fetch");

class ReportController {
  async getAuditTrail({ request, response }) {
    const { search, from, to, limit, page } = request.all();

    let q = Hit.query().orderBy("id", "desc");

    q.whereRaw(`DATE(created_at) >= "${from}" AND DATE(created_at) <= "${to}"`);

    const searchFields = ReportService.searchFieldsAuditTrail;
    const FetchInstance = new Fetch(q);
    const { status, ...data } = await FetchInstance.search(
      searchFields,
      search
    ).get(limit, page);

    return response.status(status).send(data);
  }

  async checkFarmerConsolidatedReport({ request, response }) {
    const { selected, date } = request.all();
    let res;
    const months = ReportService.getMonths(date);
    const data = await ReportService.getFarmConsolidationData(months, selected);

    if (data.length > 0)
      res = {
        message: `${data.length} entries found`,
        status: 200,
      };
    else
      res = {
        message: "No entries found.",
        status: 404,
      };

    const { status, ...body } = res;

    return response.status(status).send(body);
  }

  async getFarmerInformationByRegion({request, response}) {
    const { selectedRegions } = request.all();
    
    const regionalData = await FarmerService.getFarmersByRegion(selectedRegions);
    const farmersData = await FarmerService.getFarmersData(regionalData);

    return response.status(200).send({
      message: "Successfully fetched data",
      data: farmersData
    });
  }
}

module.exports = ReportController;
