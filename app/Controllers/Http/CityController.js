"use strict";

const City = use("App/Models/City");

class CityController {
  async getCities({ request, response }) {
    const { regionCode, provinceCode, districtCode } = request.all();

    let q = City.query();

    if (regionCode) q.where("region_code", regionCode);
    if (provinceCode) q.where("province_code", provinceCode);
    if (districtCode) q.where("district_code", districtCode);

    q.orderBy("id", "asc");

    const cities = await q.fetch();
    let data;

    if (cities.rows.length > 0)
      data = {
        message: "Successfully fetched cities.",
        data: cities.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }
}

module.exports = CityController;
