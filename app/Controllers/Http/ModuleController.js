"use strict";

const ModuleModel = use("App/Models/Module");

class ModuleController {
  async getModules({ request, response }) {
    const { platform } = request.all();

    const q = ModuleModel.query();

    platform && q.where({ platform });

    const modules = await q.fetch();
    let data;

    if (modules.rows.length > 0)
      data = {
        message: "Successfuly fetched modules.",
        data: modules.toJSON(),
        status: 200,
      };
    else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...send } = data;

    return response.status(status).send(send);
  }
}

module.exports = ModuleController;
