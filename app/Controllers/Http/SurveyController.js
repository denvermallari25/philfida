"use strict";

const Survey = use("App/Models/Survey");
const SurveyAnswer = use("App/Models/Answer");
const Drive = use("Drive");
const Fetch = use("Library/Fetch");
const SurveyService = use("App/Controllers/Service/SurveyService");
const _ = use("lodash");
const moment = use("moment");
const Socket = use("Socket");

class SurveyController {
  async getSurveyForms({ request, response }) {
    const { search, filter, limit, page } = request.all();

    let q = Survey.query().orderBy("id", "desc");

    if (filter && filter != "*") q.where("status", filter);

    const searchFields = SurveyService.searchFields;
    const FetchInstance = new Fetch(q);
    const { status, ...data } = await FetchInstance.search(
      searchFields,
      search
    ).get(limit, page);

    return response.status(status).send(data);
  }

  async getSurveyForm({ request, response }) {
    const { id } = request.all();

    const _survey = await Survey.query()
      .where({ id })
      .with("forms", (q) => {
        q.orderBy("id", "desc");
      })
      .first();
    const survey = _survey.toJSON();

    survey.forms = await Promise.all(
      survey.forms.map(async (f) => {
        const _form = await Drive.get(
          `survey/form/${f.reference_id}.json`,
          "utf8"
        );
        const form = JSON.parse(_form);

        return {
          ...f,
          questions: form,
        };
      })
    );

    return response.send({ data: survey });
  }

  async createSurveyForm({ request, response }) {
    const { name, questions } = request.all();

    const sanitizedQuestions = SurveyService.sanitizeQuestions(questions);

    const survey = await Survey.create({ name });
    const form = await survey.forms().create({});

    await Drive.put(
      `survey/form/${form.reference_id}.json`,
      JSON.stringify(sanitizedQuestions, null, 4)
    );

    SurveyService.updateVersion(survey.toJSON());

    return response.send({
      message: "Successfully created survey form.",
      data: survey.toJSON(),
    });
  }

  async updateSurveyForm({ request, response }) {
    const { id, formId, name, questions } = request.all();

    const survey = await Survey.find(id);
    survey.merge({ name });
    await survey.save();

    const _form = await survey.forms().where({ id: formId }).first();
    const sanitizedQuestions = SurveyService.sanitizeQuestions(questions);
    const isSame = await SurveyService.checkSurveyQuestionMatch(
      _form,
      sanitizedQuestions
    );

    if (!isSame) {
      const form = await survey.forms().create({});

      await Drive.put(
        `survey/form/${form.reference_id}.json`,
        JSON.stringify(sanitizedQuestions, null, 4)
      );
    }

    SurveyService.updateVersion(survey.toJSON());

    return response.send({
      message: "Successfully updated survey form.",
      data: survey.toJSON(),
    });
  }

  async updateSurveyFormStatus({ request, response }) {
    const { id, status } = request.all();

    const survey = await Survey.find(id);

    survey.merge({ status });

    await survey.save();

    SurveyService.updateVersion(survey.toJSON());

    return response.send({
      message: "Successfully updated survey form status.",
      data: survey.toJSON(),
    });
  }

  async setDefaultSurveyForm({ request, response }) {
    const { id, formId } = request.all();

    const survey = await Survey.find(id);

    survey.merge({ default_form_id: formId });

    await survey.save();

    SurveyService.updateVersion(survey.toJSON());

    return response.send({
      message: "Successfully updated default form.",
      data: survey.toJSON(),
    });
  }

  async getSurveyAnswers({ request, response, session }) {
    const { search, filter, limit, page } = request.all();

    let q = SurveyAnswer.query()
      .with("survey_form", (q) => {
        q.with("survey");
      })
      .with("farmer")
      .orderBy("id", "desc");

    if (session.role_id == 2) q.where("region_id", session.region[0].id);
    if (session.role_id == 1) q.where("status", 1);

    if (filter && filter == "0") q.where("status", 0);
    else if (filter && filter == "1") q.where("status", 1);
    else if (filter && filter == "-1") q.whereIn("status", [-1, -2]);
    else if (filter && filter == "-2") q.where("status", -10);

    const FetchInstance = new Fetch(q);
    let { status, ...data } = await FetchInstance.get(limit, page);

    if (status == 200) {
      const _data = await Promise.all(
        data.data.map(async ({ survey_form, farmer: _farmer, ...d }) => {
          const _f = await Drive.get(
            `survey/farmer/${_farmer.reference_id}.json`,
            "utf8"
          );
          const { firstName, middleName, surName } = JSON.parse(_f);

          return {
            ...d,
            survey_form: survey_form.reference_id,
            survey: survey_form.survey.name,
            farmer:
              middleName.trim().length == 0
                ? `${firstName} ${surName}`
                : `${firstName} ${middleName} ${surName}`,
          };
        })
      );

      data.data = _data;
    }

    return response.status(status).send(data);
  }

  async getSurveyAnswer({ request, response, session }) {
    const id = request.input("id");

    let s = { id };

    if (session.role_id == 2) s.region_id = session.region[0].id;
    if (session.role_id == 1) s.status = 1;

    const surveyAnswer = await SurveyAnswer.query().where(s).first();

    let res;

    if (surveyAnswer) {
      const _surveyAnswer = surveyAnswer;

      await surveyAnswer.loadMany([
        "survey_form",
        "farmer",
        "supporting_documents",
      ]);

      const surveyForm = surveyAnswer.getRelated("survey_form");
      const farmer = surveyAnswer.getRelated("farmer");

      const survey = await surveyForm.survey().fetch();
      const newSurvey = await surveyAnswer
        .new_survey()
        .with("old_survey")
        .first();
      const oldSurvey = await surveyAnswer
        .old_survey()
        .with("new_survey")
        .first();

      const _farmer = await Drive.get(
        `survey/farmer/${farmer.reference_id}.json`,
        "utf8"
      );
      const farmerJson = JSON.parse(_farmer);
      const _answer = await Drive.get(
        `survey/answer/${surveyAnswer.reference_id}.json`,
        "utf8"
      );
      const answerJson = JSON.parse(_answer);
      const _form = await Drive.get(
        `survey/form/${surveyForm.reference_id}.json`,
        "utf8"
      );
      const formJson = JSON.parse(_form);

      let data = {
        answerModel: _.omit(_surveyAnswer.toJSON(), [
          "survey_form",
          "farmer",
          "supporting_documents",
          "old_update",
          "new_update",
        ]),
        farmerModel: farmer.toJSON(),
        formModel: surveyForm.toJSON(),
        answer: answerJson,
        farmer: farmerJson,
        form: formJson,
        survey: survey.toJSON(),
      };

      if (surveyAnswer.status == -2) data.reference = oldSurvey.toJSON();
      else if (newSurvey !== null) data.reference = newSurvey.toJSON();

      if (data.answerModel.status == 1) {
        await surveyAnswer.load("approve_user");
        const approver = surveyAnswer.getRelated("approve_user");

        const photo =
          approver.profile_image != null
            ? `data:image/jpeg;base64,${await Drive.get(
                `profile/${approver.profile_image}`,
                "base64"
              )}`
            : null;

        data.approver = approver.toJSON();
        data.approverPhoto = photo;
      }

      res = {
        message: "Successfully fetched survey details.",
        survey: data,
        status: 200,
      };
    } else
      res = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = res;

    return response.status(status).send(body);
  }

  async approveSurvey({ request, response, session }) {
    const id = request.input("id");

    const surveyAnswer = await SurveyAnswer.find(id);

    surveyAnswer.merge({
      approve_date: moment().format("YYYY-MM-DD HH:mm:ss"),
      approver: session.id,
      status: 1,
    });

    await surveyAnswer.save();

    Socket.broadcastData("Answer", "Save", [surveyAnswer.toJSON()]);

    SurveyService.saveSurveysToStorage(surveyAnswer);

    return response.send({
      message: "Successfully approved survey.",
      data: surveyAnswer.toJSON(),
    });
  }

  async requestSurveyUpdate({ request, response, session }) {
    const id = request.input("id");

    const surveyAnswer = await SurveyAnswer.find(id);

    surveyAnswer.merge({
      approve_date: moment().format("YYYY-MM-DD HH:mm:ss"),
      approver: session.id,
      status: -1,
    });

    await surveyAnswer.save();

    SurveyService.updateForUpdateVersion(
      surveyAnswer.toJSON(),
      session.region[0].id
    );

    return response.send({
      message: "Successfully updated survey status.",
      data: surveyAnswer.toJSON(),
    });
  }

  async surveyDelete({ request, response, session }) {
    const id = request.input("id");

    const surveyAnswer = await SurveyAnswer.find(id);

    surveyAnswer.merge({
      approve_date: moment().format("YYYY-MM-DD HH:mm:ss"),
      approver: session.id,
      status: -10,
    });

    await surveyAnswer.save();

    Socket.broadcastData("Survey", "Save", surveyAnswer.toJSON());

    return response.send({
      message: "Successfully deleted survey.",
      data: surveyAnswer.toJSON(),
    });
  }

  async getSurveyFormKeys({ response }) {
    let data;
    const _keys = (await Drive.exists("survey/keys.json"))
      ? await Drive.get("survey/keys.json", "utf8")
      : null;

    if (_keys != null) {
      const keys = JSON.parse(_keys);

      data = {
        message: "Successfully fetched form keys.",
        keys,
        status: 200,
      };
    } else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }

  async getSurveySupportingDocuments({ request, response }) {
    let data;
    const id = request.input("id");

    const surveyAnswer = await SurveyAnswer.find(id);
    await surveyAnswer.load("supporting_documents");
    const supportingDocuments = surveyAnswer.getRelated("supporting_documents");

    if (supportingDocuments.rows.length > 0) {
      const sp = await Promise.all(
        supportingDocuments.toJSON().map(async (document) => {
          const doc = await Drive.get(
            `survey/document/${document.reference_id}.txt`,
            "utf8"
          );

          return {
            ...document,
            ...JSON.parse(doc),
          };
        })
      );

      data = {
        message: "Successfully fetched form keys.",
        supportingDocuments: sp,
        status: 200,
      };
    } else
      data = {
        message: "No data found.",
        status: 404,
      };

    const { status, ...body } = data;

    return response.status(status).send(body);
  }
}

module.exports = SurveyController;
