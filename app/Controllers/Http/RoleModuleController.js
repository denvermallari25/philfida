"use strict";
const RoleModule = use("App/Models/RoleModule");
const Role = use("App/Models/Role");

class RoleModuleController {
  async getRoleModule({ request, response }) {
    const { role } = request.all();

    const roleModule = await RoleModule.query().where("role_id", role).fetch();
    let data;

    if (roleModule.rows.length > 0)
      data = {
        status: 200,
        message: "Successfully fetched role modules.",
        data: roleModule.toJSON(),
      };
    else
      data = {
        status: 404,
        message: "No data found.",
      };

    const { status, ...body } = data;
    return response.status(200).send(body);
  }

  async updateRoleModule({ request, response }) {
    const { role, modules, platform } = request.all();

    const r = await Role.find(role);
    const mods = await r.modules().whereIn("platform", platform).fetch();

    await RoleModule.query()
      .where("role_id", role)
      .whereIn(
        "module_code",
        mods.toJSON().map((m) => m.code)
      )
      .delete();

    const roleModule = await RoleModule.createMany(
      modules.map((mod) => {
        return {
          role_id: role,
          module_code: mod,
        };
      })
    );

    const newRoleModules = roleModule.map((rm) => rm.toJSON());

    return response.status(200).send({
      message: "Successfully updated role modules",
      data: newRoleModules,
    });
  }
}

module.exports = RoleModuleController;
