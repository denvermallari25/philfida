const User = use("App/Models/User");
const Session = use("App/Models/Session");
const _ = use("lodash");
const { sign, verify } = use("jsonwebtoken");
const Config = use("Config");
const Encryption = use("Encryption");
const Drive = use("Drive");

class AuthService {
  static async checkToken(token) {
    const { secret } = Config.get("jwt");
    let tk;

    try {
      tk = verify(token, secret);
    } catch {
      tk = null;
    }

    const sessionExist = await Session.findBy("token", token);

    const session =
      tk && sessionExist != null
        ? await AuthService.getSessionByToken(token)
        : null;

    const isActive = session
      ? session.user.status == 1
        ? true
        : false
      : false;

    return tk !== null && sessionExist !== null && isActive;
  }

  static async generateToken(username) {
    const { secret, expiresIn } = Config.get("jwt");

    const { user, ...data } = await AuthService.getSession(username);

    const opts = {};

    if (user.role_id != 3) opts.expiresIn = expiresIn;

    const token = sign({ _user: Encryption.encrypt(username) }, secret, opts);

    await Session.create({ user_id: user.id, token });

    return {
      token,
      user,
      ...data,
    };
  }

  static async getSessionByToken(token, platform = "frontend") {
    const { _user } = JSON.parse(
      Buffer.from(token.split(".")[1], "base64").toString("ascii")
    );
    const username = Encryption.decrypt(_user);

    return await AuthService.getSession(username, platform);
  }

  static async getSession(username, platform = "frontend") {
    const user = await User.findBy("username", username);
    await user.load("role");
    await user.load("region");

    const role = user.getRelated("role");
    const region = user.getRelated("region");
    const modules = await role.modules().where("platform", platform).fetch();

    const photo =
      user.profile_image != null
        ? `data:image/jpeg;base64,${await Drive.get(
            `profile/${user.profile_image}`,
            "base64"
          )}`
        : null;

    return {
      user: _.omit(user.toJSON(), ["role", "region"]),
      role: role.toJSON(),
      modules: modules.toJSON().map(({ code, module }) => ({ code, module })),
      region: region ? _.omit(region.toJSON()[0], "pivot") : null,
      photo,
      first_login: user.is_password_changed == 0 ? true : false,
    };
  }

  static async getSessionRaw(token) {
    const { _user } = JSON.parse(
      Buffer.from(token.split(".")[1], "base64").toString("ascii")
    );

    const username = Encryption.decrypt(_user);
    const user = await User.findBy("username", username);
    await user.load("region");

    return user.toJSON();
  }

  static async removeToken(token) {
    const sessionExist = await Session.findBy("token", token);

    sessionExist && (await sessionExist.delete());
  }

  static async removeExpiredToken() {
    const session = await Session.all();

    const expired =
      session.rows.length > 0
        ? (
            await Promise.all(
              session
                .toJSON()
                .map(async ({ token, id }) =>
                  !(await AuthService.checkToken(token)) ? id : null
                )
            )
          ).filter((s) => s !== null)
        : [];

    expired.length > 0 &&
      (await Session.query().whereIn("id", expired).delete());
  }
}

module.exports = AuthService;
