const SurveyAnswer = use("App/Models/Answer");
const edge = use("edge.js");
const Drive = use("Drive");
const Helpers = use("Helpers");
const PDFGenerator = use("Library/PDFGenerator");
const moment = use("moment");

class PDFService {
  static async generateSurvey(id) {
    const surveyAnswer = await SurveyAnswer.find(id);
    await surveyAnswer.loadMany({
      survey_form: (builder) => builder.with("survey"),
      farmer: null,
      enumerator: null,
      region: null,
    });

    let answer = surveyAnswer.toJSON();

    const _farmer = await Drive.get(
      `survey/farmer/${answer.farmer.reference_id}.json`,
      "utf8"
    );
    const farmerJson = JSON.parse(_farmer);
    const _answer = await Drive.get(
      `survey/answer/${answer.reference_id}.json`,
      "utf8"
    );
    const answerJson = JSON.parse(_answer);
    const _form = await Drive.get(
      `survey/form/${answer.survey_form.reference_id}.json`,
      "utf8"
    );
    const formJson = JSON.parse(_form);

    const _consolidatedSurvey = formJson.map((fj) => {
      const { entry } = answerJson.find((aj) => aj.id == fj.id);
      return { ...fj, entry };
    });

    // const farmLocation = answerJson.find((aj) => aj.label == "Farm Location");
    // const farmLocationIdx = answerJson.findIndex(
    //   (aj) => aj.label == "Farm Location"
    // );
    // answerJson.splice(farmLocationIdx, 1);
    // const farmAreaIdx = answerJson.findIndex((aj) => aj.label == "Farm Area");
    // answerJson.splice(farmAreaIdx, 1);

    const consolidatedSurvey = _consolidatedSurvey.filter(
      (cs) =>
        !["PhilippineStandardGeographicCode", "KeyholeMarkupLanguage"].includes(
          cs.input_type
        )
    );

    const _answers = consolidatedSurvey.map((aj) => {
      const ansRack = [];

      if (aj.entry) {
        aj.entry.forEach((a) => {
          let _a = a.label;

          if (a.otherTextField) _a += `: ${a.otherTextField}`;

          ansRack.push(_a);
        });
      }

      return { answer: ansRack.join(", "), ...aj };
    });

    const answers = _answers.reduce((acc, _, i) => {
      if (i % 4 === 0) acc.push(_answers.slice(i, i + 4));
      return acc;
    }, []);

    const lastSize = answers[answers.length - 1].length;

    if (lastSize != 4) {
      const missing = 4 - lastSize;
      for (let index = 0; index < missing; index++) {
        answers[answers.length - 1].push({});
      }
    }

    answer.answerData = answers;
    answer.farmerData = farmerJson;

    answer.logo = await Drive.get(
      Helpers.resourcesPath("img/logo.png"),
      "base64"
    );

    const html = await Drive.get(Helpers.viewsPath("pdf/farmer.edge"), "utf8");
    const rawHTML = edge.renderString(html, answer);

    const PDFGeneratorInstance = new PDFGenerator(rawHTML);
    const pdf = await PDFGeneratorInstance.generateBuffer();

    return pdf;
  }

  static async generateFarmerInfoConsolidation(data, selected) {
    const groupedEnumerator = Object.values(
      data.reduce(PDFService.filterByEnumerator, {})
    );

    let consolidated = {};

    let enumIndex = 0;

    consolidated.content = groupedEnumerator.map(({ name, data }) => {
      const _enumIndex = enumIndex;
      enumIndex = enumIndex + data.length;

      return {
        enumerator: name,
        index: _enumIndex,
        dataCount: data.length,
        data: PDFService.filterByAddress(data, selected, _enumIndex),
      };
    });

    consolidated.totals = PDFService.getConsolidatedTotal(consolidated.content);

    consolidated.logo = await Drive.get(
      Helpers.resourcesPath("img/logo.png"),
      "base64"
    );

    let total = [];

    for (let index = 0; index < data.length; index++) {
      total.push(index);
    }

    consolidated.totalCount = total;
    consolidated.selected = selected;

    const html = await Drive.get(
      Helpers.viewsPath("pdf/farmerconsolidation.edge"),
      "utf8"
    );
    const rawHTML = edge.renderString(html, consolidated);
    const PDFGeneratorInstance = new PDFGenerator(rawHTML, null, null, true);
    const pdf = await PDFGeneratorInstance.generateBuffer();

    return pdf;
  }

  static filterByEnumerator(_consolidated, _data) {
    let content = {
      surveyAnswer: _data.surveyAnswers,
      surveyFarmer: PDFService.getFarmerDetails(_data.farmer),
      crop: _data.surveyModel.survey_form.survey.name,
      timestamp: moment(
        _data.surveyModel.created_at_mobile,
        "MMMM D, YYYY"
      ).format("MM/DD/YYYY"),
    };

    if (!_consolidated[_data.surveyModel.enumerator.id])
      _consolidated[_data.surveyModel.enumerator.id] = {
        name: _data.surveyModel.enumerator.fullName,
        data: [content],
      };
    else _consolidated[_data.surveyModel.enumerator.id].data.push(content);

    return _consolidated;
  }

  static getFarmerDetails(farmer) {
    const { firstName, middleName, surName, extesionName, birthDate } = farmer;
    let name = [];

    firstName.trim().length > 0 && name.push(firstName);
    middleName.trim().length > 0 && name.push(middleName);
    surName.trim().length > 0 && name.push(surName);
    extesionName.trim().length > 0 && name.push(extesionName);

    const age = moment().diff(moment(birthDate, "YYYY-MM-DD"), "years");

    return { name: name.join(" "), age };
  }

  static filterByAddress(data, selected, startIndex) {
    const addr = {};

    for (let index = 0; index < data.length; index++) {
      const loc = data[index].surveyAnswer.find(
        (sa) => sa.properties.key == "farm_location"
      );

      const _region = loc.entry
        .find((l) => l.type == "Region")
        .label.toUpperCase();
      const __province = loc.entry
        .find((l) => l.type == "Province")
        .label.toUpperCase();
      const _province = __province.length ? __province : "N/A";
      const _municipality = loc.entry
        .find((l) => l.type == "Municipality")
        .label.toUpperCase();
      const _city = loc.entry.find((l) => l.type == "City").label.toUpperCase();
      const _muncity = _municipality.length > 0 ? _municipality : _city;
      const _barangay = loc.entry
        .find((l) => l.type == "Barangay")
        .label.toUpperCase();

      const content = {
        index: index + startIndex,
        farmer: data[index].surveyFarmer,
        crop: data[index].crop,
        surveyDate: data[index].timestamp,
        answer: selected.reduce((_answer, val) => {
          const entry = data[index].surveyAnswer.find(
            (sa) => sa.properties.key == val.key
          );

          _answer[val.key] = PDFService.getAnswer(entry);

          return _answer;
        }, {}),
      };

      if (!addr[_region]) {
        addr[_region] = {
          region: _region,
          index: index + startIndex,
          dataCount: 1,
          data: {},
        };
      } else addr[_region].dataCount = addr[_region].dataCount + 1;

      if (!addr[_region].data[_province]) {
        addr[_region].data[_province] = {
          province: _province,
          index: index + startIndex,
          dataCount: 1,
          data: {},
        };
      } else
        addr[_region].data[_province].dataCount =
          addr[_region].data[_province].dataCount + 1;

      if (!addr[_region].data[_province].data[_muncity]) {
        addr[_region].data[_province].data[_muncity] = {
          muncity: _muncity,
          index: index + startIndex,
          dataCount: 1,
          data: {},
        };
      } else
        addr[_region].data[_province].data[_muncity].dataCount =
          addr[_region].data[_province].data[_muncity].dataCount + 1;

      if (!addr[_region].data[_province].data[_muncity].data[_barangay]) {
        addr[_region].data[_province].data[_muncity].data[_barangay] = {
          barangay: _barangay,
          index: index + startIndex,
          dataCount: 1,
          content: [content],
        };
      } else {
        addr[_region].data[_province].data[_muncity].data[_barangay].dataCount =
          addr[_region].data[_province].data[_muncity].data[_barangay]
            .dataCount + 1;
        addr[_region].data[_province].data[_muncity].data[
          _barangay
        ].content.push(content);
      }
    }

    return addr;
  }

  static getAnswer(_entry) {
    const ansRack = [];

    if (_entry.entry) {
      _entry.entry.forEach((a) => {
        let _a = a.label;

        if (a.otherTextField) _a += `: ${a.otherTextField}`;

        ansRack.push(_a);
      });
    }

    return ansRack.join(", ");
  }

  static getConsolidatedTotal(data) {
    let totalProvince = 0;
    let totalMuncity = 0;
    let totalBarangay = 0;
    let totalFarmer = 0;

    data.forEach(({ data: _data }) => {
      Object.values(_data).forEach(({ data: region }) => {
        totalProvince++;

        Object.values(region).forEach(({ data: muncity }) => {
          totalMuncity++;

          Object.values(muncity).forEach(({ data: barangay }) => {
            totalBarangay += Object.values(barangay).length;
            totalFarmer += Object.values(barangay).length;
          });
        });
      });
    });

    return {
      totalProvince,
      totalMuncity,
      totalBarangay,
      totalFarmer,
    };
  }
}

module.exports = PDFService;
