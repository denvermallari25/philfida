const User = use("App/Models/User");

class RegionService {
  static get searchFields() {
    return ["code", "name", "region_name"];
  }

  static async getRegionUser(type) {
    const role_id = type == "administrator" ? 2 : 3;

    return await User.query()
      .where({ role_id, status: 1 })
      .doesntHave("region")
      .orderBy("id", "desc")
      .fetch();
  }
}

module.exports = RegionService;
