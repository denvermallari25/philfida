const Drive = use("Drive");
const Socket = use("Socket");

class AnswerService {
  static async updateVersion(documents) {
    Socket.broadcastData("SupportingDocument", "Save", documents);

    const version = await Drive.get("survey/document_version", "utf8");
    await Drive.put("survey/document_version", `${+version + 1}`);
  }
}

module.exports = AnswerService;
