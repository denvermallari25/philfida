const User = use("App/Models/User");
const Drive = use("Drive");
const moment = use ("moment");

class FarmerService {
    static async updateVersion() {
        const version = await Drive.get("survey/farmer_version", "utf8");
        await Drive.put("survey/farmer_version", `${+version + 1}`);
    }

    static async getFarmersByRegion(selectedRegions) {
        const data = await Promise.all(
          selectedRegions.map(async (region) => {
            const query = User.query();
            query.with("farmers");
            query.with("userRegion", userRegionQuery => {
              userRegionQuery.where("region_code", region.regionCode);
            }).where("role_id", 3);
    
            const _data = (await query.fetch()).toJSON();
            let ret = [];
    
            _data.forEach(enumerator => {
              if (enumerator.userRegion) {
                const farmers = enumerator.farmers;
                
                for (let index = 0; index < farmers.length; index++) {
                  ret.push({
                    reference_id: farmers[index].reference_id,
                    original_id: farmers[index].original_id
                  });
                }
              }
            });
            
            return { 
              region_code: region.regionCode,
              region_name: region.regionName, 
              regionalFarmerData: ret 
            };
          })
        );
    
        return data;
      }

      static async getFarmersData(regionalData) {
        let satanizedData = [];
        let ret = [];
        
        regionalData.forEach(async (region) => {
          const regionData = region.regionalFarmerData;
          const data = await Promise.all(
            regionData.map(async (d) => {
              const _data = (await Drive.exists(`survey/farmer/${d.reference_id}.json`))
                ? await Drive.get(`survey/farmer/${d.reference_id}.json`, "utf8")
                : null;
                
                return _data ? JSON.parse(_data) : null;
            })
          );
          
          if (regionData.length > 0) {
            for (let index = 0; index < data.length; index++) {
              if (data[index].address.region == region.region_name.toUpperCase()) {
                let name = [];

                data[index].firstName.trim().length > 0 && name.push(data[index].firstName);
                data[index].middleName.trim().length > 0 && name.push(data[index].middleName);
                data[index].surName.trim().length > 0 && name.push(data[index].surName);
                data[index].extesionName.trim().length > 0 && name.push(data[index].extesionName);
                
                satanizedData.push({
                  fullname: `${name.join(" ")}`,
                  address: data[index].address,
                  affiliation: data[index].affiliationGroupName,
                  civilStatus: data[index].civilStatus,
                  educationalAttainment: data[index].educationalAttainment,
                  age: moment().diff(data[index].birthDate, "years", false)
                });
              }
            }
            
            ret.push({
              region_code: region.region_code,
              regional_data: satanizedData
            });
          }
        });
    
        return ret;
      }
}

module.exports = FarmerService;
