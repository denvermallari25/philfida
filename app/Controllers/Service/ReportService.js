const User = use("App/Models/User");

const moment = use("moment");
const Drive = use("Drive");

class ReportService {
  static get searchFieldsAuditTrail() {
    return [
      "transaction_id",
      "username",
      "ip",
      "url",
      "module",
      "method",
      "description",
      "status",
    ];
  }

  static getMonths(date) {
    const [from, to] = date.split(" - ");
    let months = [];

    let _from = from;

    while (moment(_from, "MMMM YYYY").isSameOrBefore(moment(to, "MMMM YYYY"))) {
      months.push(moment(_from, "MMMM YYYY").format("MMMM-YYYY").toUpperCase());
      _from = moment(_from, "MMMM YYYY").add(1, "M").format("MMMM YYYY");
    }

    return months;
  }

  static async getFarmConsolidationData(months, selected) {
    const labels = selected.map((s) => s.key);

    let data = await Promise.all(
      months.map(async (m) => {
        const _data = (await Drive.exists(`survey/consolidated/${m}.json`))
          ? await Drive.get(`survey/consolidated/${m}.json`, "utf8")
          : null;

        return _data ? JSON.parse(_data) : null;
      })
    );

    let sanitizedData = [];

    for (let index = 0; index < data.length; index++) {
      if (data[index]) {
        const contents = data[index].filter((d) => {
          return d.surveyAnswers.some((_d) =>
            labels.includes(_d.properties.key)
          );
        });

        sanitizedData = [...sanitizedData, ...contents];
      }
    }

    return sanitizedData;
  }
}

module.exports = ReportService;