const Mail = use("Mail");
const Helpers = use("Helpers");

class UserService {
  static get searchFields() {
    return ["username", "email", "first_name", "last_name", "middle_name"];
  }

  static mailConfig(type) {
    let subject;
    let template;

    if (type == "create") {
      subject = "Account Credentials";
      template = "emails.createUser";
    }

    return {
      subject,
      template,
    };
  }

  static async sendEmail(type, data, to) {
    const { template, subject } = UserService.mailConfig(type);

    await Mail.send(template, data, (message) => {
      message
        .embed(Helpers.resourcesPath("img/header.jpg"), "header")
        .to(to)
        .from("iaansanity@gmail.com", "PHILFIDA")
        .subject(subject);
    });
  }
}

module.exports = UserService;
