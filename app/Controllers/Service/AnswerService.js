const Drive = use("Drive");
const Socket = use("Socket");
const { v4: uuidv4 } = use("uuid");

class AnswerService {
  static async updateVersion(answers) {
    Socket.broadcastData("Answer", "Save", answers);

    const version = await Drive.get("survey/answer_version", "utf8");
    await Drive.put("survey/answer_version", `${+version + 1}`);
  }
}

module.exports = AnswerService;
