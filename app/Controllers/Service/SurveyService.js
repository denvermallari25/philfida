const snakeCaseKeys = use("snakecase-keys");
const Drive = use("Drive");
const Socket = use("Socket");
const moment = use("moment");

class SurveyService {
  static get searchFields() {
    return ["name"];
  }

  static sanitizeQuestions(questions) {
    return questions.map(({ id, inputType, properties: _properties }) => {
      const {
        label,
        key,
        isRequired,
        psgc,
        entries: _entries,
        inputType: _inputType,
      } = _properties;
      let properties = {
        label,
        key,
        isRequired,
      };

      if (_inputType) properties.inputType = _inputType;

      if (inputType == "PhilippineStandardGeographicCode")
        properties.psgc = psgc;
      else if (["DropDown", "CheckBox", "RadioButton"].includes(inputType)) {
        const entries = _entries.map(
          ({ id, label, hasTextField, hasTextFieldType }) => ({
            id,
            label,
            hasTextField,
            hasTextFieldType,
          })
        );

        properties.entries = entries;
      }

      return snakeCaseKeys(
        {
          id,
          inputType,
          properties,
        },
        { deep: true }
      );
    });
  }

  static async checkSurveyQuestionMatch(formCollection, newQuestion) {
    const _formQuestions = await Drive.get(
      `survey/form/${formCollection.reference_id}.json`,
      "utf8"
    );

    const formQuestions = JSON.stringify(JSON.parse(_formQuestions));
    const questions = JSON.stringify(newQuestion);

    return formQuestions == questions;
  }

  static async updateVersion(survey) {
    Socket.broadcastData("Survey", "Save", survey);

    const version = await Drive.get("survey/version", "utf8");
    await Drive.put("survey/version", `${+version + 1}`);
  }

  static async updateForUpdateVersion(survey, region_id) {
    Socket.broadcastData("Survey", "Save", survey);

    const _v = await Drive.get("survey/update_version", "utf8");
    let v = JSON.parse(_v);

    if (v[region_id] == undefined) {
      v = {
        [region_id]: 1,
      };
    } else {
      v[region_id] = v[region_id] + 1;
    }

    await Drive.put("survey/update_version", JSON.stringify(v, null, 4));
  }

  static async keyHelper(model) {
    const { reference_id } = model;

    const _form = await Drive.get(`survey/form/${reference_id}.json`, "utf8");
    const form = JSON.parse(_form);

    let keys = form.map((f) => f.properties.key);

    const _listKeys = (await await Drive.exists("survey/keys.json"))
      ? await Drive.get("survey/keys.json", "utf8")
      : null;

    if (_listKeys != null) {
      let listKeys = JSON.parse(_listKeys);

      const newKeys = keys.filter((k) => !listKeys.includes(k));

      listKeys = newKeys.length ? [...listKeys, ...newKeys] : listKeys;

      keys = listKeys;
    }

    await Drive.put("survey/keys.json", JSON.stringify(keys, null, 4));
  }

  static async saveSurveysToStorage(surveyAnswer) {
    await surveyAnswer.loadMany({
      survey_form: (builder) => builder.with("survey"),
      farmer: null,
      enumerator: null,
      region: null,
    });

    const _sa = surveyAnswer.toJSON();
    const dy = moment(_sa.created_at_mobile, "MMMM D, YYYY h:mm:ss A")
      .format("MMMM-YYYY")
      .toUpperCase();

    let storageData = (await Drive.exists(`survey/consolidated/${dy}.json`))
      ? JSON.parse(await Drive.get(`survey/consolidated/${dy}.json`, "utf-8"))
      : [];

    const farmerJSON = JSON.parse(
      await Drive.get(`survey/farmer/${_sa.farmer.reference_id}.json`, "utf8")
    );
    const answerJSON = JSON.parse(
      await Drive.get(`survey/answer/${_sa.reference_id}.json`, "utf8")
    );
    const formJSON = JSON.parse(
      await Drive.get(
        `survey/form/${_sa.survey_form.reference_id}.json`,
        "utf8"
      )
    );

    const consolidatedSurvey = formJSON.map((fj) => {
      const { entry } = answerJSON.find((aj) => aj.id == fj.id);
      return { ...fj, entry };
    });

    const cont = {
      surveyAnswers: consolidatedSurvey,
      farmer: farmerJSON,
      surveyModel: _sa,
    };

    storageData.push(cont);

    await Drive.put(
      `survey/consolidated/${dy}.json`,
      JSON.stringify(storageData, null, 4)
    );
  }
}

module.exports = SurveyService;
