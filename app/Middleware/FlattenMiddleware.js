"use strict";
const camelcaseKeys = use("camelcase-keys");

class Flatten {
  async handle({ response, request }, next) {
    await next();
    const url = request.originalUrl();
    const [platform, requestModule, requestMethod] = url
      .split("/")
      .filter(Boolean)
      .filter((url, idx) => idx == 1 || idx == 3 || idx == 4);

    if (!["pdf"].includes(requestModule ? requestModule.toLowerCase() : requestModule)) {
      const serialized = camelcaseKeys(response.lazyBody.content, {
        deep: true,
      });

      response.send(serialized);
    }
  }
}

module.exports = Flatten;
