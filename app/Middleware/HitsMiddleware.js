"use strict";
const Logger = use("Logger");
const moment = use("moment");
const { v5: uuidv5 } = use("uuid");
const HitsModel = use("App/Models/Hit");
const { sentenceCase } = use("change-case");
const AuthService = use("App/Controllers/Service/AuthService");

class Hits {
  static descriptions() {
    return {
      //auth
      "auth-login": "User logs in",
      "auth-getsession": "Fetch user session",
      "auth-reconnect": "User reconnects",
      "auth-logout": "User logs out",
      //role
      "role-getroles": "Fetch roles",
      //user
      "user-getusers": "Fetch users",
      "user-getuser": "Fetch user",
      "user-uploadphoto": "Upload user profile photo",
      "user-createuser": "Create user",
      "user-updateuser": "Update user",
      "user-changepassword": "Change user password",
      "user-updateuserstatus": "Update user status",
      //module
      "module-getmodules": "Fetch modules",
      //role module
      "roleModule-updaterolemodule": "Updated role modules",
      "roleModule-getrolemodule": "Fetch role modules",
      //region
      "region-getregionsraw": "Fetch regions",
      "region-getregions": "Fetch regions",
      "region-getregion": "Fetch region",
      "region-getunassignedadministrators": "Fetch unassigned administrators",
      "region-updateregionadministrator": "Update region administrator.",
      "region-getunassignedenumerators": "Fetch unassigned enumerators",
      "region-assignregionenumerator": "Assign region enumerators",
      "region-unassignregionenumerator": "Unassign region enumerators",
      //province
      "province-getprovinces": "Fetch provinces",
      //district
      "district-getdistricts": "Fetch districts",
      //municipality
      "municipality-getmunicipalities": "Fetch municipalities",
      //city
      "city-getcities": "Fetch cities",
      //barangay
      "barangay-getbarangays": "Fetch barangays",
      //survey
      "survey-getsurveyforms": "Fetch survey forms",
      "survey-getsurveyform": "Fetch survey form",
      "survey-createsurveyform": "Create survey form",
      "survey-updatesurveyform": "Update survey form",
      "survey-updatesurveyformstatus": "Update survey form status",
      "survey-setdefaultsurveyform": "Set default survey form",
      "survey-getsurveyanswers": "Fetch survey answers",
      "survey-getsurveyanswer": "Fetch survey answer",
      "survey-approvesurvey": "Approve survey",
      "survey-requestsurveyupdate": "Request survey update",
      "survey-getsurveyformkeys": "Get survey form keys",
      "survey-surveydelete": "Delete survey answer",
      "survey-getsurveysupportingdocuments": "Get survey supporting documents",
      //sync
      "sync-surveyforms": "Sync survey forms",
      "sync-getversion": "Get sync version",
      "sync-syncfarmers": "Sync farmers",
      "sync-syncanswers": "Sync answers",
      "sync-syncsupportingdocuments": "Sync supporting documents",
      "sync-getsurveyforupdate": "Get survey for update",
      //report
      "report-getaudittrail": "Get audit trail",
      "report-checkfarmerconsolidatedreport":
        "Check farmer consolidated report",
      "report-getfarmerinformationbyregion": "Get farmer information by region",
      //pdf
      "pdf-viewpdf": "View PDF",
      //session
      "session-hassession": "Fetch has session",
      "session-forcelogout": "Force log out",
    };
  }

  static ignoreUrl() {
    return ["auth-login"];
  }

  async handle({ request, response }, next) {
    await next();
    const token = request.header("Authorization")
      ? request.header("Authorization").replace("Bearer ", "")
      : null;

    const Hit = new HitsModel();
    const uuid = uuidv5(moment().format("x"), uuidv5.URL);
    const url = request.originalUrl();
    const [platform, requestModule, requestMethod] = url
      .split("/")
      .filter(Boolean)
      .filter((url, idx) => idx == 1 || idx == 3 || idx == 4);
    const session =
      !Hits.ignoreUrl().includes(
        `${requestModule}-${requestMethod}`.toLowerCase()
      ) &&
      token !== null &&
      (await AuthService.checkToken(token))
        ? await AuthService.getSessionRaw(token)
        : null;

    if (requestModule && requestMethod) {
      if (platform === "mobile") {
          Hit.merge({
          transaction_id: uuid,
          username: session ? session.username : "",
          ip: request.ip(),
          url,
          module: sentenceCase(requestModule).toUpperCase(),
          method: sentenceCase(requestMethod).toUpperCase(),
          description: Hits.descriptions()[
            `${requestModule}-${requestMethod}`.toLowerCase()
          ],
          status: response.response.statusCode,
        });

        await Hit.save();
      }

      Hit.merge({
        transaction_id: uuid,
        username: session ? session.username : "",
        ip: request.ip(),
        url,
        module: sentenceCase(requestModule).toUpperCase(),
        method: sentenceCase(requestMethod).toUpperCase(),
        description: Hits.descriptions()[
          `${requestModule}-${requestMethod}`.toLowerCase()
        ],
        status: response.response.statusCode,
      });

      await Hit.save();

      Logger.transport("hits").info({
        timestamp: moment().format("MMMM D, YYYY - h:mm:ss A"),
        referenceId: uuid,
        request: request.all(),
        response: response.lazyBody.content,
        url,
      });

      const body = response.lazyBody.content;
      if (["pdf"].includes(requestModule.toLowerCase())) response.send(body);
      else response.send({ ...body, txId: uuid });
    }
  }
}

module.exports = Hits;
