"use strict";
const AuthService = use("App/Controllers/Service/AuthService");

class Auth {
  async handle(ctx, next) {
    const { request, response } = ctx;

    const token = request.header("Authorization")
      ? request.header("Authorization").replace("Bearer ", "")
      : null;

    if (token !== null && (await AuthService.checkToken(token))) {
      ctx.session = await AuthService.getSessionRaw(token);
      await next();
    } else response.status(401).json({ message: "Unauthenticated user." });
  }
}

module.exports = Auth;
