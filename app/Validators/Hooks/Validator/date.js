const Database = use("Database");
const moment = use("moment");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
    const value = get(data, field);

    if (!value) return;

    if (!moment(value, "YYYY-MM-DD", true).isValid()) throw message;
};

hook.name = "date";

module.exports = hook;
