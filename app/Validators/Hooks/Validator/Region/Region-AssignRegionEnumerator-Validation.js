const Region = use("App/Models/Region");
const User = use("App/Models/User");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { id, enumerators } = data;

  if (!id) return;

  const region = await Region.find(id);

  if (!region) return;

  const users = await User.query().whereIn("id", enumerators).fetch();

  if (users.rows.length != enumerators.length)
    throw "Enumerator/s does not exist.";

  if (!users.toJSON().every((u) => u.role_id == 3))
    throw "Enumerator/s selected is not an enumerator account.";

  const hasRegion = await User.query()
    .whereIn("id", enumerators)
    .doesntHave("region")
    .fetch();

  if (hasRegion.rows.length != enumerators.length)
    throw "Enumerator/s is/are already assigned to other region.";
};

hook.name = "Region-AssignRegionEnumerator-Validation";

module.exports = hook;
