const Region = use("App/Models/Region");
const User = use("App/Models/User");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { id, enumerators } = data;

  if (!id) return;

  const region = await Region.find(id);

  if (!region) return;

  const users = await User.query().whereIn("id", enumerators).fetch();

  if (users.rows.length != enumerators.length)
    throw "Enumerator/s does not exist.";

  if (!users.toJSON().every((u) => u.role_id == 3))
    throw "Enumerator/s selected is not an enumerator account.";

  const inRegion = await region
    .users()
    .whereIn("users.id", enumerators)
    .fetch();

  if (inRegion.rows.length != enumerators.length)
    throw "Enumerator/s is/are not assigned to this region.";
};

hook.name = "Region-UnassignRegionEnumerator-Validation";

module.exports = hook;
