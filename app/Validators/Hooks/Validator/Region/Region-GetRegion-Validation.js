let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { id, code } = data;

  if (id != undefined && code != undefined)
    throw "Please use only one identifier.";

  if (id == undefined && code == undefined) throw "Invalid request.";
};

hook.name = "Region-GetRegion-Validation";

module.exports = hook;
