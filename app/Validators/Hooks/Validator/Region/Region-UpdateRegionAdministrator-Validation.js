const Region = use("App/Models/Region");
const User = use("App/Models/User");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { id, administrator } = data;

  if (!id) return;

  const region = await Region.find(id);
  const user = await User.find(administrator);

  if (!region || !user) return;

  if (user.role_id != 2)
    throw "Administrator selected is not an administrator account.";

  const reg = await user.region().first();

  if (reg && region.id == reg.id) throw "No changes made.";

  if (reg) throw "Administrator already assigned to other region.";
};

hook.name = "Region-UpdateRegionAdministrator-Validation";

module.exports = hook;
