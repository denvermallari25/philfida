const User = use("App/Models/User");
const Hash = use("Hash");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { username, password, customValidation: url } = data;
  const platform = url
    .split("/")
    .filter(Boolean)
    .find((p, i) => i == 1);

  if (!username || !password) return;

  const _user = await User.findBy("username", username);
 
  if (!_user) throw "Invalid credentials.";
  
  await _user.loadMany(["region", "sessions"]);
  
  let isInvalid = false;

  const isSame = await Hash.verify(password, _user.password);
  const user = _user.toJSON();
  
  if (user.status == -1)
    throw "Account is locked. Please contact system administrator.";
  else if (user.status == 0)
    throw "Account is inactive. Please contact system administrator";
  else if (!isSame) isInvalid = true;
  else if (
    (platform == "web" && user.role_id == 3) ||
    (platform == "mobile" && user.role_id != 3)
  )
    throw "Invalid credentials.";
  else if (
    platform == "mobile" &&
    user.role_id == 3 &&
    user.sessions.length > 0
  )
    throw "Account already logged in.";
  else if (user.role_id != 1 && user.region.length == 0)
    throw "Your account is not assigned to a region. Please contact system administrator.";

  if (isInvalid) {
    const login_attempts = _user.login_attempts + 1;

    const merge = {
      login_attempts,
    };

    if (login_attempts == 3) merge.status = -1;
    _user.merge(merge);
    await _user.save();
    throw "Invalid credentials.";
  }
};

hook.name = "Auth-Login-Validation.js";

module.exports = hook;
