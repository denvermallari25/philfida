const Database = use("Database");
const { snakeCase } = use("change-case");

let hook = {};

hook.fn = async (_data, field, message, args, get) => {
  let data = _data;

  const { model: table, fields, plugin } = get(data, field);

  const [fieldName, ...exclude] = args;

  const fieldValue = data[fieldName];

  const row = await Database.table(table).where(fieldName, fieldValue).first();
  if (!row) return;

  exclude.push("hasRowChanges");
  const rack = Object.keys(data)
    .map((field) => (fields.includes(field) ? field : null))
    .map((field) => (!exclude.includes(field) ? field : null))
    .filter(Boolean)
    .reduce((a, b) => {
      a[b] = data[b];

      return a;
    }, {});

  const isSame = Object.keys(rack).every((idx) => {
    const snakeCasedKey = snakeCase(idx);
    const entryValue = rack[idx];

    return row[snakeCasedKey] == entryValue;
  });

  if (plugin) {
    const plug = use(`ValidatorHooks/Validator/${plugin}`);

    if (await plug(_data, isSame)) throw message;
  } else if (isSame) throw message;
};

hook.name = "hasRowChanges";

module.exports = hook;
