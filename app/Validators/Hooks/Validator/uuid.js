const { validate } = require("uuid");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const value = get(data, field);

  if (!value) return;

  if (!validate(value)) throw message;
};

hook.name = "uuid";

module.exports = hook;
