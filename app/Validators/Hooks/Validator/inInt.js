let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const value = get(data, field);

  if (!value) return;

  const rack = args.join(", ");

  if (!args.map((arg) => +arg).includes(value)) throw eval("`" + message + "`");
};

hook.name = "inInt";

module.exports = hook;
