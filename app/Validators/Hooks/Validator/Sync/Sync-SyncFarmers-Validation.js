const Farmer = use("App/Models/Farmer");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { farmers } = data;

  if (!farmers.every(({ id }) => id != undefined))
    throw "Every object must contain (id)";

  const id = farmers.map(({ id }) => id);
  const farmer = await Farmer.query().whereIn("original_id", id).fetch();

  if (farmer.rows.length > 0) throw "Farmer/s already exist.";
};

hook.name = "Sync-SyncFarmers-Validation";

module.exports = hook;
