const Answer = use("App/Models/Answer");
const Farmer = use("App/Models/Farmer");
const SurveyForm = use("App/Models/SurveyForm");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { survey_answers } = data;

  if (survey_answers == undefined) return;

  if (
    !survey_answers.every(
      ({ id, farmer_id, survey_form_id }) =>
        id != undefined && farmer_id != undefined && survey_form_id != undefined
    )
  )
    throw "Every object must contain (id, farmer_id, survey_form_id)";

  const ids = await Answer.query()
    .whereIn(
      "original_id",
      survey_answers.map(({ id }) => id)
    )
    .fetch();

  if (ids.rows.length > 0) throw "Survey answer/s already exist.";

  const farmerIds = [
    ...new Set(survey_answers.map(({ farmer_id }) => farmer_id)),
  ];

  const checkFarmerIds = await Farmer.query()
    .whereIn("original_id", farmerIds)
    .fetch();

  if (checkFarmerIds.rows.length != farmerIds.length)
    throw "Farmer/s does not exist.";

  const surveyFormIds = [
    ...new Set(survey_answers.map(({ survey_form_id }) => survey_form_id)),
  ];

  const checkSurveyFormIds = await SurveyForm.query()
    .whereIn("id", surveyFormIds)
    .fetch();

  if (checkSurveyFormIds.rows.length != surveyFormIds.length)
    throw "Survey form/s does not exist.";
};

hook.name = "Sync-SyncAnswers-Validation";

module.exports = hook;
