const Answer = use("App/Models/Answer");
const SupportingDocument = use("App/Models/SupportingDocument");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { supporting_documents } = data;

  if (supporting_documents == undefined) return;

  if (
    !supporting_documents.every(
      ({ id, surveyAnswerId }) => id != undefined && surveyAnswerId != undefined
    )
  )
    throw "Every object must contain (id, surveyAnswerId)";

  const surveyAnswerId = [
    ...new Set(
      supporting_documents.map(({ surveyAnswerId }) => surveyAnswerId)
    ),
  ];

  const answer = await Answer.query()
    .whereIn("original_id", surveyAnswerId)
    .fetch();

  if (answer.rows.length != surveyAnswerId.length)
    throw "Answer/s does not exist.";

  const id = supporting_documents.map(({ id }) => id);

  const sp = await SupportingDocument.query()
    .whereIn("original_id", id)
    .fetch();

  if (sp.rows.length > 0) throw "Document/s already exist.";
};

hook.name = "Sync-SyncSupportingDocumentsValidation-Validation";

module.exports = hook;
