const Survey = use("App/Models/Survey");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { id, formId } = data;

  if (!formId || !id) return;

  const survey = await Survey.find(id);
  const form = await survey.forms().where({ id: formId }).first();

  if (!form) throw "Form does not exist.";
  if (survey.default_form_id == formId)
    throw "This is already the default form.";
};

hook.name = "Survey-SetDefaultSurveyForm-Validation";

module.exports = hook;
