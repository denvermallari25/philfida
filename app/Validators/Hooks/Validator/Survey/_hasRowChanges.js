const Drive = use("Drive");
const Survey = use("App/Models/Survey");
const snakeCaseKeys = use("snakecase-keys");
const SurveyService = use("App/Controllers/Service/SurveyService");

module.exports = async (data, isSame) => {
  const { questions: _questions, id, formId } = data;

  if (!id) return false;
  if (!_questions || !Array.isArray(_questions)) return false;

  const survey = await Survey.find(id);
  const form = await survey.forms().where({ id: formId }).first();

  if (!form) throw "Form does not exist.";

  const questions = SurveyService.sanitizeQuestions(_questions);
  const isQuestionsSame = await SurveyService.checkSurveyQuestionMatch(
    form,
    questions
  );

  return isQuestionsSame && isSame ? true : false;
};
