let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { questions } = data;
  
  const checkProperties = questions.every(({ inputType, properties }) => {
    if (
      inputType == "PhilippineStandardGeographicCode" &&
      properties &&
      (!properties.psgc ||
        !Array.isArray(properties.psgc) ||
        properties.psgc.length == 0)
    )
      return false;
    else if (
      ["DropDown", "CheckBox", "RadioButton"].includes(inputType) &&
      properties &&
      (!properties.entries ||
        !Array.isArray(properties.entries) ||
        properties.entries.length == 0)
    )
      return false;
    else return true;
  });

  if (!checkProperties) throw "Invalid properties";
};

hook.name = "Survey-CreateSurveyForm-Validation";

module.exports = hook;
