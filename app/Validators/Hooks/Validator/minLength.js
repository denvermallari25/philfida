let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const value = get(data, field);

  if (!value) return;

  const [length] = args;
  const label = Array.isArray(value) ? "element/s" : "character/s long";

  if (`${value}`.length < length) throw eval("`" + message + "`");
};

hook.name = "minLength";

module.exports = hook;
