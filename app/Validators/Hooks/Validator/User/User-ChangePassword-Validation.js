const User = use("App/Models/User");
const Hash = use("Hash");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const oldPassword = data["oldPassword"];
  const newPassword = data["newPassword"];

  const { user } = await getSessionByToken(data["token"]);

  if (oldPassword) {
    const usr = await User.find(user.id);
    const isSame = await Hash.verify(oldPassword, usr.password);

    if (!isSame) throw "Old password does not match your current password.";
  }

  if (newPassword) {
    const usr = await User.find(user.id);
    const isSame = await Hash.verify(newPassword, usr.password);

    if (isSame) throw "New password is the same as the current password.";
  }
};

hook.name = "User-ChangePassword-Validation";

module.exports = hook;
