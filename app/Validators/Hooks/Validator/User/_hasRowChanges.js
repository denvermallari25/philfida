module.exports = (data, isSame) => {
  return !data.isImageUpdated && isSame;
};
