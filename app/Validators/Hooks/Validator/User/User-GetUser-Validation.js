let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const { id, username } = data;

  if (id != undefined && username != undefined)
    throw "Please use only one identifier.";

  if (id == undefined && username == undefined) throw "Invalid request.";
};

hook.name = "User-GetUser-Validation";

module.exports = hook;
