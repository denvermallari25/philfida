const Database = use("Database");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const value = get(data, field);

  if (!value) return;

  const [table, column, ...additional] = args;

  const row = await Database.table(table);

  if (row.length == 0) throw "Internal server error.";

  const rack = [...row.map((r) => `${r.id}`), ...additional];

  if (!rack.map((r) => r).includes(value)) throw eval("`" + message + "`");
};

hook.name = "inFilter";

module.exports = hook;
