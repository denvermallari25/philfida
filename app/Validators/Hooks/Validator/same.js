let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const value = get(data, field);
  const match = args;

  if (!value) return;

  if (value != data[match]) throw message;
};

hook.name = "same";

module.exports = hook;
