const Database = use("Database");

let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const value = get(data, field);

  if (!value) return;

  if (!Array.isArray(value)) return;

  const isInts = value.every((v) => typeof v === "string");

  if (!isInts) throw message;
};

hook.name = "arrayString";

module.exports = hook;
