let hook = {};

hook.fn = async (data, field, message, args, get) => {
  const value = get(data, field);
  
  if(!value) return;

  if (value.length == 0) return;

  const rack = args.join(", ");

  if (!value.every((v) => rack.includes(v))) throw eval("`" + message + "`");
};

hook.name = "inArrayString";

module.exports = hook;
