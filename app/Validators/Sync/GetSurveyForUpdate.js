"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class SyncGetSurveyForUpdate {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules } = await getSessionByToken(token, "backend");

    if (!modules.map((m) => m.code).includes(405))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }
}

module.exports = SyncGetSurveyForUpdate;
