"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const Answer = use("App/Models/Answer");

class SyncSyncAnswers {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules, region } = await getSessionByToken(token, "backend");
    const { survey_answers } = this.ctx.request.all();
    const froms = survey_answers.filter((sa) => sa.from).map((sa) => sa.from);
    let exist = null;

    if (froms.length) {
      exist = await Answer.query()
        .whereIn("original_id", froms)
        .where({ region_id: region.id, status: -1 })
        .fetch();
    }
  
    if (!modules.map((m) => m.code).includes(403))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });
    else if (froms.length > 0 && exist.rows.length != froms.length)
      return this.ctx.response.status(403).json({
        message:
          "The survey/s you want to update is either does not exist in this region or not for updating.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      survey_answers: "required|array|maxLength:10",
      customValidation: "Sync-SyncAnswers-Validation",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = SyncSyncAnswers;
