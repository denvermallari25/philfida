"use strict";
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class SyncSurveyForms {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules } = await getSessionByToken(token, "backend");

    if (!modules.map((m) => m.code).includes(400))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }
}

module.exports = SyncSurveyForms;
