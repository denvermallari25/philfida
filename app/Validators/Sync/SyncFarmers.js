"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class SyncSyncFarmers {
    async authorize() {
        const token = this.ctx.request
            .header("Authorization")
            .replace("Bearer ", "");

        const { modules } = await getSessionByToken(token, "backend");

        if (!modules.map((m) => m.code).includes(402))
            return this.ctx.response.status(403).json({
                message: "You are not allowed access this method.",
            });

        return true;
    }

    get validateAll() {
        return true;
    }

    get rules() {
        return {
            farmers: "required|array|maxLength:10",
            customValidation: "Sync-SyncFarmers-Validation",
        };
    }

    get messages() {
        return generateValidationMessage(this.rules);
    }

    async fails(err) {
        const errorMessages = err.map(({ message }) => message);

        return this.ctx.response.status(400).send({ errorMessages });
    }
}

module.exports = SyncSyncFarmers;
