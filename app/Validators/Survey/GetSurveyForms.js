"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class SurveyGetSurveyForms {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules } = await getSessionByToken(token, "backend");

    if (!modules.map((m) => m.code).includes(301))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      search: "string",
      filter: "string|inString:*,0,1",
      limit: "integer",
      page: "integer",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      search: "trim",
      filter: "trim",
      limit: "trim|toInt",
      page: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = SurveyGetSurveyForms;
