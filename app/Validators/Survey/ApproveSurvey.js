"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const SurveyAnswer = use("App/Models/Answer");

class SurveyApproveSurvey {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { id } = this.ctx.request.all();
    const { modules, region } = await getSessionByToken(token, "backend");
    const answer = await SurveyAnswer.find(id);

    if (!modules.map((m) => m.code).includes(312))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });
    else if (answer && answer.region_id != region.id)
      return this.ctx.response.status(403).json({
        message: "You are not allowed to approve this survey.",
      });
    else if (answer && answer.status != 0)
      return this.ctx.response.status(403).json({
        message: "Survey already updated.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      id: "required|integer|entryExistInModel:survey_answers,id",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = SurveyApproveSurvey;
