"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class SurveyCreateSurveyForm {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules } = await getSessionByToken(token, "backend");

    if (!modules.map((m) => m.code).includes(303))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      name: "required|string|maxLength:255",
      "questions.*.id": "required|uuid",
      "questions.*.inputType":
        "required|string|inString:TextField,Email,ContactNumber,DatePicker,DropDown,CheckBox,RadioButton,ImagePicker,KeyholeMarkupLanguage,PhilippineStandardGeographicCode",
      "questions.*.properties.label": "required|string",
      "questions.*.properties.key": "required|string",
      "questions.*.properties.isRequired": "required|boolean",
      "questions.*.properties.psgc":
        "array|arrayString|inArrayString:region,province,district,municipality,city,barangay",
      "questions.*.properties.entries.*.id": "required|uuid",
      "questions.*.properties.entries.*.label": "required|string",
      "questions.*.properties.entries.*.hasTextField": "required|boolean",
      customValidation: "Survey-CreateSurveyForm-Validation",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      name: "trim",
      "questions.*.inputType": "trim",
      "questions.*.properties.label": "trim",
      "questions.*.properties.key": "trim",
      "questions.*.properties.entries.*.label": "trim",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = SurveyCreateSurveyForm;
