"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const SurveyAnswer = use("App/Models/Answer");
const User = use("App/Models/User");
const Hash = use("Hash");

class SurveySurveyDelete {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { id, password } = this.ctx.request.all();
    const { modules, region, user } = await getSessionByToken(token, "backend");
    const answer = await SurveyAnswer.find(id);
    const _user = await User.findBy("username", user.username);
    const passwordPassed = await Hash.verify(password, _user.password);

    if (!modules.map((m) => m.code).includes(313))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });
    else if (!passwordPassed) {
      return this.ctx.response.status(403).json({
        message: "Password is incorrect.",
      });
    } else if (answer && answer.region_id != region.id)
      return this.ctx.response.status(403).json({
        message: "You are not allowed to update this survey.",
      });
    else if (answer && answer.status != 0)
      return this.ctx.response.status(403).json({
        message: "Survey already updated.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      id: "required|integer|entryExistInModel:survey_answers,id",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = SurveySurveyDelete;
