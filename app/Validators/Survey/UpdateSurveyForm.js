"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class SurveyUpdateSurveyForm {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules } = await getSessionByToken(token, "backend");

    if (!modules.map((m) => m.code).includes(304))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }

  get data() {
    const body = this.ctx.request.all();

    return {
      ...body,
      hasRowChanges: {
        model: "surveys",
        fields: Object.keys(this.rules),
        plugin: "Survey/_hasRowChanges.js",
      },
    };
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      id: "required|integer|entryExistInModel:surveys,id",
      formId: "required|integer",
      name: "required|string|maxLength:255",
      "questions.*.id": "required|uuid",
      "questions.*.inputType":
        "required|string|inString:TextField,Email,ContactNumber,DatePicker,DropDown,CheckBox,RadioButton,ImagePicker,KeyholeMarkupLanguage,PhilippineStandardGeographicCode",
      "questions.*.properties.label": "required|string",
      "questions.*.properties.key": "required|string",
      "questions.*.properties.isRequired": "required|boolean",
      "questions.*.properties.psgc":
        "array|arrayString|inArrayString:region,province,district,municipality,city,barangay",
      "questions.*.properties.entries.*.id": "required|uuid",
      "questions.*.properties.entries.*.label": "required|string",
      "questions.*.properties.entries.*.hasTextField": "required|boolean",
      customValidation: "Survey-CreateSurveyForm-Validation",
      hasRowChanges: "hasRowChanges:id,formId,questions",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
      formId: "trim|toInt",
      name: "trim",
      "questions.*.inputType": "trim",
      "questions.*.properties.label": "trim",
      "questions.*.properties.key": "trim",
      "questions.*.properties.entries.*.label": "trim",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = SurveyUpdateSurveyForm;
