"use strict";
const { checkToken, getSessionByToken } = use(
  "App/Controllers/Service/AuthService"
);
const Answer = use("App/Models/Answer");

class RegionAssignRegionEnumerator {
  async authorize() {
    const { token, type, data } = this.ctx.params;

    if (!(token != undefined || type != undefined || data != undefined))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this page.",
      });
    else if (!(await checkToken(token)))
      return this.ctx.response.status(403).json({
        message: "Unauthenticated user.",
      });
    else if (
      !["survey", "farmerinfoconsolidation"].includes(type.toLowerCase())
    )
      return this.ctx.response.status(403).json({
        message: "Invalid type.",
      });

    if (type == "survey") {
      const _d = Buffer.from(data, "base64").toString();
      const { id } = JSON.parse(_d);

      if (id) {
        const { region } = await getSessionByToken(token, "backend");
        const answer = await Answer.query()
          .where({
            id,
            region_id: region.id,
          })
          .first();

        if (!answer)
          return this.ctx.response.status(403).json({
            message: "No data found.",
          });
      } else
        return this.ctx.response.status(403).json({
          message: "Invalid data.",
        });
    }

    return true;
  }
}

module.exports = RegionAssignRegionEnumerator;
