"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class ModuleGetModules {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules } = await getSessionByToken(token, "backend");

    if (!modules.map((m) => m.code).includes(301))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      platform: "string|inString:backend,frontend",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      platform: "trim",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = ModuleGetModules;
