"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const User = use("App/Models/User");

class SessionForceLogOut {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { id } = this.ctx.request.all();
    const { modules, region } = await getSessionByToken(token, "backend");

    const _user = await User.find(id);
    await _user.load("region");
    const user = _user.toJSON();

    if (!modules.map((m) => m.code).includes(701))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });
    else if (user.role_id != 3)
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });
    else if (user.region.length == 0 || user.region[0].id != region.id)
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      id: "required|integer|entryExistInModel:users,id",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = SessionForceLogOut;
