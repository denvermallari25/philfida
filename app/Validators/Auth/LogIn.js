"use strict";
const { generateValidationMessage } = use("Library/Serializer");

class AuthLogin {
  get data() {
    const body = this.ctx.request.all();

    return {
      ...body,
      customValidation: this.ctx.request.originalUrl(),
    };
  }

  get validateAll() {
    return false;
  }

  get rules() {
    return {
      username: "required|string|matchRegex:^\\S*$",
      password: "required|string|matchRegex:^\\S*$",
      customValidation: "Auth-Login-Validation.js",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      username: "toUpper"
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = AuthLogin;
