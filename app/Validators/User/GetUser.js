"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const User = use("App/Models/User");

class UserGetUser {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules, role, user } = await getSessionByToken(token, "backend");
    const { id, username } = this.ctx.request.all();
    const [_identifier, _id] = [id ? "id" : "username", id || username];
    const _user = await User.findBy(_identifier, _id);

    const allowed = modules.map((m) => m.code).includes(102);
    let unauthorized = false;
    let message = "";

    if (!allowed) {
      unauthorized = true;
      message = "You are not allowed access this method.";
    } else if (
      role.id == 1 &&
      user[_identifier] != _user[_identifier] &&
      _user.role_id == 3
    ) {
      unauthorized = true;
      message = "You are not allowed to view enumerator account.";
    } else if (
      role.id == 2 &&
      user[_identifier] != _user[_identifier] &&
      [1, 2].includes(_user.role_id)
    ) {
      unauthorized = true;
      message = "You are not allowed to view administrative account.";
    } else if (role.id == 3 && user[_identifier] != _user[_identifier]) {
      unauthorized = true;
      message = "You are not allowed to view other account.";
    }

    if (unauthorized)
      return this.ctx.response.status(403).json({
        message,
      });

    return true;
  }

  get validateAll() {
    return false;
  }

  get rules() {
    return {
      customValidation: "User-GetUser-Validation",
      id: "integer|entryExistInModel:users,id",
      username: "string|entryExistInModel:users,username",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
      username: "trim",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = UserGetUser;
