"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const User = use("App/Models/User");

class UserUpdateUser {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules, role, user } = await getSessionByToken(token, "backend");
    const { id } = this.ctx.request.all();
    const allowed = modules.map((m) => m.code).includes(103);
    const _user = await User.find(id);
    let unauthorized = false;
    let message = "";

    if (!allowed) {
      unauthorized = true;
      message = "You are not allowed access this method.";
    } else if (
      allowed &&
      role.id == 1 &&
      user.id != _user.id &&
      _user.role_id == 3
    ) {
      unauthorized = true;
      message = "You are not allowed to update enumerator account.";
    } else if (
      allowed &&
      role.id == 2 &&
      user.id != _user.id &&
      _user.role_id != 3
    ) {
      unauthorized = true;
      message = "You are not allowed to update administrative account.";
    } else if (role.id == 3 && user.id != _user.id) {
      unauthorized = true;
      message = "You are not allowed to view other account.";
    }

    if (unauthorized)
      return this.ctx.response.status(403).json({
        message,
      });

    return true;
  }

  get data() {
    const body = this.ctx.request.all();

    return {
      ...body,
      hasRowChanges: {
        model: "users",
        fields: Object.keys(this.rules),
        plugin: "User/_hasRowChanges.js",
      },
    };
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      id: "required|integer|entryExistInModel:users,id",
      email: "required|email",
      firstName: "required|string|maxLength:255",
      middleName: "string|maxLength:255",
      extensionName: "string|maxLength:255",
      lastName: "required|string|maxLength:255",
      isImageUpdated: "required|boolean",
      hasPhoto: "required|boolean",
      hasRowChanges: "hasRowChanges:id,isImageUpdated,hasPhoto",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
      email: "trim",
      firstName: "trim",
      middleName: "trim",
      extensionName: "trim",
      lastName: "trim",
      roleId: "trim|toInt",
      status: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = UserUpdateUser;
