"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const User = use("App/Models/User");

class UserUploadPhoto {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules, role, user } = await getSessionByToken(token, "backend");
    const { id } = this.ctx.request.all();
    const allowed = modules.map((m) => m.code).includes(105);
    const _user = await User.find(id);
    let unauthorized = false;
    let message = "";

    if (!allowed) {
      unauthorized = true;
      message = "You are not allowed access this method.";
    } else if (
      allowed &&
      role.id == 1 &&
      user.id != _user.id &&
      _user.role_id == 3
    ) {
      unauthorized = true;
      message = "You are not allowed to update enumerator account.";
    } else if (
      allowed &&
      role.id == 2 &&
      user.id != _user.id &&
      _user.role_id != 3
    ) {
      unauthorized = true;
      message = "You are not allowed to update administrative account.";
    } else if (role.id == 3 && user.id != _user.id) {
      unauthorized = true;
      message = "You are not allowed to view other account.";
    }

    if (unauthorized)
      return this.ctx.response.status(403).json({
        message,
      });

    return true;
  }

  get data() {
    const body = this.ctx.request.all();

    return {
      ...body,
      hasRowChanges: "users",
    };
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      id: "required|integer|entryExistInModel:users,id",
      profile: "file_types:image",
    };
  }

  get messages() {
    return {
      ...generateValidationMessage(this.rules),
      "profile.file_types": "Profile photo must be an image.",
    };
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = UserUploadPhoto;
