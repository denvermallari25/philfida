"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");

class UserCreateUser {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules, role } = await getSessionByToken(token, "backend");
    const { roleId } = this.ctx.request.all();
    const allowed = modules.map((m) => m.code).includes(103);
    let unauthorized = false;
    let message = "";

    if (!allowed) {
      unauthorized = true;
      message = "You are not allowed access this method.";
    } else if (allowed && role.id == 1 && roleId == 3) {
      unauthorized = true;
      message = "You are not allowed to create enumerator account.";
    } else if (allowed && role.id == 2 && roleId != 3) {
      unauthorized = true;
      message = "You are not allowed to create administrative account.";
    }

    if (unauthorized)
      return this.ctx.response.status(403).json({
        message,
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      username:
        "required|string|minLength:4|maxLength:16|matchRegex:^\\S*$|entryUniqueInModel:users,username",
      email: "required|email",
      firstName: "required|string|maxLength:255",
      middleName: "string|maxLength:255",
      extensionName: "string|maxLength:255",
      lastName: "required|string|maxLength:255",
      roleId: "required|integer|entryExistInModel:roles,id",
      regionId: "integer|entryExistInModel:regions,id",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      username: "trim|toUpper",
      email: "trim",
      firstName: "trim",
      middleName: "trim",
      extensionName: "trim",
      lastName: "trim",
      roleId: "trim|toInt",
      regionId: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = UserCreateUser;
