"use strict";
const { generateValidationMessage } = use("Library/Serializer");
const { getSessionByToken } = use("App/Controllers/Service/AuthService");
const User = use("App/Models/User");

class UserUpdateUserStatus {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { user, modules, role } = await getSessionByToken(token, "backend");
    const { id } = this.ctx.request.all();
    const allowed = modules.map((m) => m.code).includes(106);
    const _user = await User.find(id);
    let unauthorized = false;
    let message = "";

    if (!allowed) {
      unauthorized = true;
      message = "You are not allowed access this method.";
    } else if (
      allowed &&
      role.id == 1 &&
      user.id != _user.id &&
      _user.role_id == 3
    ) {
      unauthorized = true;
      message = "You are not allowed to update enumerator account.";
    } else if (
      allowed &&
      role.id == 2 &&
      user.id != _user.id &&
      _user.role_id != 3
    ) {
      unauthorized = true;
      message = "You are not allowed to update administrative account.";
    } else if (role.id == 3 && user.id != _user.id) {
      unauthorized = true;
      message = "You are not allowed to view other account.";
    }

    if (unauthorized)
      return this.ctx.response.status(403).json({
        message,
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get rules() {
    return {
      id: "required|integer|entryExistInModel:users,id",
      status: "required|integer|inInt:0,1",
    };
  }

  get messages() {
    return generateValidationMessage(this.rules);
  }

  get sanitizationRules() {
    return {
      id: "trim|toInt",
      status: "trim|toInt",
    };
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = UserUpdateUserStatus;
