"use strict";
const { generateValidationMessage } = use("Library/Serializer");

class UserChangePassword {
  async authorize() {
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");

    const { modules } = await getSessionByToken(token, "backend");

    if (!modules.map((m) => m.code).includes(107))
      return this.ctx.response.status(403).json({
        message: "You are not allowed access this method.",
      });

    return true;
  }

  get validateAll() {
    return true;
  }

  get data() {
    const body = this.ctx.request.all();
    const token = this.ctx.request
      .header("Authorization")
      .replace("Bearer ", "");
    return {
      ...body,
      token,
    };
  }

  get rules() {
    return {
      oldPassword: `required|string`,
      newPassword: `required|string|minLength:4|maxLength:16|matchRegex:^\\S*$`,
      repeatNewPassword: "required|string|same:newPassword",
      customValidation: "User-ChangePassword-Validation",
    };
  }

  get messages() {
    return {
      ...generateValidationMessage(this.rules),
      "repeatNewPassword.same":
        "Repeat new password must be the same value with new password",
    };
  }

  get sanitizationRules() {
    return {};
  }

  async fails(err) {
    const errorMessages = err.map(({ message }) => message);

    return this.ctx.response.status(400).send({ errorMessages });
  }
}

module.exports = UserChangePassword;
