"use strict";
const Logger = use("Logger");
const moment = use("moment");
const BaseExceptionHandler = use("BaseExceptionHandler");

class ExceptionHandler extends BaseExceptionHandler {
  async handle(error, { response }) {
 
    response.status(500).json({
      message: "Internal server error.",
    });
  }
  
  async report(error, { request }) {
    if (["ECONNABORTED"].includes(error.code)) return;
    
    Logger.transport("error").error({
      timestamp: moment().format("MMMM D, YYYY - h:mm:ss A"),
      error: error.stack.replace(/\n\s+/gm, " "),
      request: request.all(),
      url: request.url(),
      method: request.method(),
      headers: request.headers()
    });
  }
}

module.exports = ExceptionHandler;
